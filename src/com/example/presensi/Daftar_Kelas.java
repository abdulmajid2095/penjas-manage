package com.example.presensi;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Daftar_Kelas extends Activity implements OnRefreshListener, OnItemClickListener, OnClickListener, OnItemLongClickListener{

	private SwipeRefreshLayout swipeRefreshLayout;
	ListView hasil_kelas;
	Dataku data;
	String[] id_kelas;
	String[] nama_kelas;
	String[] semester;
	String[] tahun;
	String[] kkm;
	Button tambah;
	String kelasbaru,semesterbaru,tahunbaru,kkmbaru;
	private ProgressDialog pDialog;
	int pilihan,dipilih;
	
	TextView kosong;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_daftar__kelas);
		
		data = Dataku.findById(Dataku.class, 1L);
		
		Intent kelas = getIntent();
		pilihan = Integer.parseInt(kelas.getStringExtra("pilihan"));
		
		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.kelas_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		hasil_kelas = (ListView)findViewById(R.id.hasil_kelas);
        hasil_kelas.setOnItemClickListener(this);
        hasil_kelas.setOnItemLongClickListener(this);
        
        
        tambah = (Button) findViewById(R.id.tbl_tambah);
        tambah.setOnClickListener(this);
        
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu ...");
        pDialog.setCancelable(true);
        ambil_data();
        
        kosong = (TextView) findViewById(R.id.teks_kosong);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		if(pilihan==1)
		{
			Intent intent = new Intent(Daftar_Kelas.this,Absensi_tampil.class);
			intent.putExtra("id_kelas", ""+id_kelas[posisi]);
			intent.putExtra("nama_kelas", ""+nama_kelas[posisi]);
			intent.putExtra("tahun", "Semester "+semester[posisi]+" - Tahun Ajaran "+tahun[posisi]);
			startActivity(intent);
		}
		if(pilihan==2)
		{
			Intent intent = new Intent(Daftar_Kelas.this,Penilaian_tampil.class);
			intent.putExtra("id_kelas", ""+id_kelas[posisi]);
			intent.putExtra("nama_kelas", ""+nama_kelas[posisi]);
			intent.putExtra("tahun", "Semester "+semester[posisi]+" - Tahun Ajaran "+tahun[posisi]);
			intent.putExtra("kkm", ""+kkm[posisi]);
			startActivity(intent);
		}
		if(pilihan==3)
		{
			Intent intent = new Intent(Daftar_Kelas.this,Daftar_Siswa.class);
			intent.putExtra("id_kelas", ""+id_kelas[posisi]);
			intent.putExtra("nama_kelas", ""+nama_kelas[posisi]);
			startActivity(intent);
		}
		
		
	}

	

	
private void ambil_data() {
    	
    	String json_alamat = "http://tolong.hol.es/penilaian/daftar_kelas.php?id="+data.idku;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        try {
                        	
                        	int jumlah_data = response.length();
                        	if(jumlah_data==0)
                        	{
                        		kosong.setVisibility(View.VISIBLE);
                        	}
                        	else
                        	{
                        		kosong.setVisibility(View.GONE);
                        	}
                            
                        	id_kelas = new String[jumlah_data];
                        	nama_kelas = new String[jumlah_data];
                        	semester = new String[jumlah_data];
                        	tahun = new String[jumlah_data];
                        	kkm = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	id_kelas[i] = hasil.getString("id_kelas");
                                    nama_kelas[i] = hasil.getString("nama_kelas");
                                    semester[i] = hasil.getString("semester");
                                    tahun[i] = hasil.getString("tahunajaran");
                                    kkm[i] = hasil.getString("kkm");
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Hasil tidak ditemukan",
                                    Toast.LENGTH_LONG).show();
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_Kelas adapter = new Adapter_Kelas(getApplicationContext(), nama_kelas);
                		hasil_kelas.setAdapter(adapter);
                		int x = hasil_kelas.getChildCount();
                        for(int r =0;r<x;r++)
                        {
                        	hasil_kelas.getChildAt(r).setBackgroundResource(R.drawable.edit);
                        }
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }

@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	ambil_data();
}

@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.tbl_tambah:
		dialog_tambah();
		break;

	}
}


private void dialog_tambah() {

    // custom dialog
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_tambah_kelas);
    final EditText kelasku = (EditText) dialog.findViewById(R.id.dialog_tambah_kelas_edittext);
    final EditText semesterku = (EditText) dialog.findViewById(R.id.dialog_tambah_kelas_edittext2);
    final EditText tahunku = (EditText) dialog.findViewById(R.id.dialog_tambah_kelas_edittext3);
    final EditText kkmku = (EditText) dialog.findViewById(R.id.dialog_tambah_kelas_edittext4);
    Button simpan = (Button) dialog.findViewById(R.id.dialog_tambah_kelas_simpan);
    simpan.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			kelasbaru = kelasku.getText().toString();
			kelasbaru = kelasbaru.replace(' ','+');
			semesterbaru = semesterku.getText().toString();
			semesterbaru = semesterbaru.replace(' ','+');
			tahunbaru = tahunku.getText().toString();
			tahunbaru = tahunbaru.replace(' ','+');
			kkmbaru = kkmku.getText().toString();
			kkmbaru = kkmbaru.replace(' ','+');
			tambah_kelas();
			showpDialog();
			dialog.dismiss();	
		}
	});
    dialog.show();

}


public void tambah_kelas()
{
	String url = "http://tolong.hol.es/penilaian/tambah_kelas.php?nama="+kelasbaru+"&id="+data.idku+"&semester="+semesterbaru+"&tahun="+tahunbaru+"&kkm="+kkmbaru;
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="1";
                            String cek2="2";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penambahan Kelas Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penambahan Kelas Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Periksa sambungan anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
    
    
}

private void showpDialog() {
    if (!pDialog.isShowing())
        pDialog.show();
}

private void hidepDialog() {
    if (pDialog.isShowing())
        pDialog.dismiss();
}

@Override
public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int posisi,
		long arg3) {
	// TODO Auto-generated method stub
	dipilih=posisi;
	pilihan();
	return true;
}
	
public void pilihan()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_pilihan);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    Button edit = (Button) dialog.findViewById(R.id.edit);
    edit.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			edit();
		}
	});
    Button hapus = (Button) dialog.findViewById(R.id.hapus);
    hapus.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus();
		}
	});
    
	dialog.show();
}


public void hapus()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_hapus);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    TextView teks = (TextView) dialog.findViewById(R.id.hapus_teks);
    teks.setText("Hapus Kelas "+nama_kelas[dipilih]+" ?");
    Button ya = (Button) dialog.findViewById(R.id.ya);
    ya.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus_kelas();
		}
	});
    Button tidak = (Button) dialog.findViewById(R.id.tidak);
    tidak.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
		}
	});
    
	dialog.show();
}

public void edit()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_edit_kelas);
    final EditText tx = (EditText) dialog.findViewById(R.id.edittext);
    tx.setText(""+nama_kelas[dipilih]);
    final EditText tx2 = (EditText) dialog.findViewById(R.id.edittext2);
    tx2.setText(""+semester[dipilih]);
    final EditText tx3 = (EditText) dialog.findViewById(R.id.edittext3);
    tx3.setText(""+tahun[dipilih]);
    final EditText tx4 = (EditText) dialog.findViewById(R.id.edittext4);
    tx4.setText(""+kkm[dipilih]);
    
    Button spn = (Button) dialog.findViewById(R.id.simpan);
    spn.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			kelasbaru = tx.getText().toString();
			kelasbaru = kelasbaru.replace(' ','+');
			semesterbaru = tx2.getText().toString();
			semesterbaru = semesterbaru.replace(' ','+');
			tahunbaru = tx3.getText().toString();
			tahunbaru = tahunbaru.replace(' ','+');
			kkmbaru = tx4.getText().toString();
			kkmbaru = kkmbaru.replace(' ','+');
			dialog.dismiss();
			simpan_edit();
		}
	});
    
    
	dialog.show();
}


public void simpan_edit()
{
	String url = "http://tolong.hol.es/penilaian/update_kelas.php?nama="+kelasbaru+"&id="+id_kelas[dipilih]+"&semester="+semesterbaru+"&tahun="+tahunbaru+"&kkm="+kkmbaru;
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="0";
                            String cek2="1";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Update Kelas Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Update Kelas Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Periksa sambungan anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
}

public void hapus_kelas()
{
	String url = "http://tolong.hol.es/penilaian/hapus_kelas.php?id="+id_kelas[dipilih];
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="0";
                            String cek2="1";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Hapus Kelas Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Hapus Kelas Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Periksa sambungan anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
}

}
