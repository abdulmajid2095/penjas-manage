package com.example.presensi;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Daftar_RPP extends Activity implements OnRefreshListener, OnItemClickListener, OnClickListener, OnItemLongClickListener{

	private SwipeRefreshLayout swipeRefreshLayout;
	ListView hasil_kelas;
	Dataku data;
	String[] id_rpp;
	String[] nama_rpp;
	String[] nama_file;
	Button tambah;
	String judulbaru;
	private ProgressDialog pDialog;
	int pilihan,dipilih;
	
	TextView kosong;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_daftar__rpp);
		
		data = Dataku.findById(Dataku.class, 1L);
		
		
		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.kelas_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		hasil_kelas = (ListView)findViewById(R.id.hasil_kelas);
        hasil_kelas.setOnItemClickListener(this);
        hasil_kelas.setOnItemLongClickListener(this);
        
        
        tambah = (Button) findViewById(R.id.tbl_tambah);
        tambah.setOnClickListener(this);
        
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu ...");
        pDialog.setCancelable(true);
        ambil_data();
        
        kosong = (TextView) findViewById(R.id.teks_kosong);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://docs.google.com/gview?embedded=true&url=tolong.hol.es/penilaian/file/"+nama_file[posisi]));
		startActivity(intent);
		
	}

	

	
private void ambil_data() {
    	
    	String json_alamat = "http://tolong.hol.es/penilaian/daftar_rpp.php?id="+data.idku;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        try {
                        	
                        	int jumlah_data = response.length();
                        	if(jumlah_data==0)
                        	{
                        		kosong.setVisibility(View.VISIBLE);
                        	}
                        	else
                        	{
                        		kosong.setVisibility(View.GONE);
                        	}
                            
                        	id_rpp = new String[jumlah_data];
                        	nama_rpp = new String[jumlah_data];
                        	nama_file = new String[jumlah_data];
                        	
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	id_rpp[i] = hasil.getString("id_upload");
                                    nama_rpp[i] = hasil.getString("judul");
                                    nama_file[i] = hasil.getString("nama");
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Hasil tidak ditemukan",
                                    Toast.LENGTH_LONG).show();
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_Kelas adapter = new Adapter_Kelas(getApplicationContext(), nama_rpp);
                		hasil_kelas.setAdapter(adapter);
                		int x = hasil_kelas.getChildCount();
                        for(int r =0;r<x;r++)
                        {
                        	hasil_kelas.getChildAt(r).setBackgroundResource(R.drawable.edit);
                        }
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }

@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	ambil_data();
}

@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.tbl_tambah:
		Intent intent = new Intent(Daftar_RPP.this,RPP2.class);
		startActivity(intent);
		break;

	}
}


private void showpDialog() {
    if (!pDialog.isShowing())
        pDialog.show();
}

private void hidepDialog() {
    if (pDialog.isShowing())
        pDialog.dismiss();
}

@Override
public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int posisi,
		long arg3) {
	// TODO Auto-generated method stub
	dipilih=posisi;
	pilihan();
	return true;
}
	

public void pilihan()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_pilihan);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    Button edit = (Button) dialog.findViewById(R.id.edit);
    edit.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			edit();
		}
	});
    Button hapus = (Button) dialog.findViewById(R.id.hapus);
    hapus.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus();
		}
	});
    
	dialog.show();
}

public void hapus()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_hapus);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    TextView teks = (TextView) dialog.findViewById(R.id.hapus_teks);
    teks.setText("Hapus "+nama_rpp[dipilih]+" ?");
    Button ya = (Button) dialog.findViewById(R.id.ya);
    ya.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus_rpp();
		}
	});
    Button tidak = (Button) dialog.findViewById(R.id.tidak);
    tidak.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
		}
	});
    
	dialog.show();
}


public void edit()
{
	final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_edit_rpp);
    final EditText tx = (EditText) dialog.findViewById(R.id.edittext);
    tx.setText(""+nama_rpp[dipilih]);
    
    Button spn = (Button) dialog.findViewById(R.id.simpan);
    spn.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			judulbaru = tx.getText().toString();
			judulbaru = judulbaru.replace(' ','+');
			dialog.dismiss();
			simpan_edit();
		}
	});
    
    
	dialog.show();
}


public void simpan_edit()
{
	String url = "http://tolong.hol.es/penilaian/update_rpp.php?judul="+judulbaru+"&id="+id_rpp[dipilih];
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="0";
                            String cek2="1";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Update Kelas Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Update Kelas Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Periksa sambungan anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
}


public void hapus_rpp()
{
	String url = "http://tolong.hol.es/penilaian/hapus_rpp.php?id="+id_rpp[dipilih];
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="0";
                            String cek2="1";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Hapus RPP Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Hapus RPP Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Periksa sambungan anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
}

}
