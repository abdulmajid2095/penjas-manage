package com.example.presensi;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_absensi extends ArrayAdapter<String> {

	String[] nama={};
	String[] nis={};
	int[] p1={}, p2={}, p3={}, p4={}, p5={}, p6={}, p7={}, p8={}, p9={}, p10={}, p11={}, p12={}, p13={}, p14={}, p15={}, p16={}, p17={}, p18={}, p19={}, p20={}, p21={}, p22={}, p23={}, p24={};
	Context c;
	LayoutInflater inflater;
	
	
	
	public Adapter_absensi(Context context, String[]nama, String[]nis, int[]p1, int[]p2, int[]p3, int[]p4, int[]p5, int[]p6, int[]p7, int[]p8, int[]p9, int[]p10, int[]p11, int[]p12, int[]p13, int[]p14, int[]p15, int[]p16, int[]p17, int[]p18, int[]p19, int[]p20, int[]p21, int[]p22, int[]p23, int[]p24) {
		super(context, R.layout.model_absensi,nama);
		
		this.c = context;
		this.nama = nama;
		this.nis = nis;
		this.p1=p1;
		this.p2=p2;
		this.p3=p3;
		this.p4=p4;
		this.p5=p5;
		this.p6=p6;
		this.p7=p7;
		this.p8=p8;
		this.p9=p9;
		this.p10=p10;
		this.p11=p11;
		this.p12=p12;
		this.p13=p13;
		this.p14=p14;
		this.p15=p15;
		this.p16=p16;
		this.p17=p17;
		this.p18=p18;
		this.p19=p19;
		this.p20=p20;
		this.p21=p21;
		this.p22=p22;
		this.p23=p23;
		this.p24=p24;
				
	}
	
	public class ViewHolder
	{
		TextView nama,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,p24;
		TextView nis,sakit,ijin,alfa,hadir,persen;
		TextView no;
		
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_absensi,null);
			
		}
		final ViewHolder holder = new ViewHolder();
		int x=position+1;
		holder.no = (TextView) convertView.findViewById(R.id.siswa_no);
		holder.no.setText(""+x);
		holder.nama = (TextView) convertView.findViewById(R.id.siswa_nama);
		holder.nama.setText(""+nama[position]);
		holder.nis = (TextView) convertView.findViewById(R.id.siswa_nis);
		holder.nis.setText(""+nis[position]);
		
		int isakit=0,iijin=0,ialfa=0,ihadir=0;
		
		holder.p1 = (TextView) convertView.findViewById(R.id.pertemuan1);
		if(p1[position]==0)
		{
			holder.p1.setText("");
		}
		else if(p1[position]==1)
		{
			holder.p1.setText("v");
			ihadir=ihadir+1;
		}
		else if(p1[position]==2)
		{
			holder.p1.setText("S");
			isakit=isakit+1;
		}
		else if(p1[position]==3)
		{
			holder.p1.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p1.setText("A");
			ialfa=ialfa+1;
		}
		
		
		holder.p2 = (TextView) convertView.findViewById(R.id.pertemuan2);
		if(p2[position]==0)
		{
			holder.p2.setText("");
		}
		else if(p2[position]==1)
		{
			holder.p2.setText("v");
			ihadir=ihadir+1;
		}
		else if(p2[position]==2)
		{
			holder.p2.setText("S");
			isakit=isakit+1;
		}
		else if(p2[position]==3)
		{
			holder.p2.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p2.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p3 = (TextView) convertView.findViewById(R.id.pertemuan3);
		if(p3[position]==0)
		{
			holder.p3.setText("");
		}
		else if(p3[position]==1)
		{
			holder.p3.setText("v");
			ihadir=ihadir+1;
		}
		else if(p3[position]==2)
		{
			holder.p3.setText("S");
			isakit=isakit+1;
		}
		else if(p3[position]==3)
		{
			holder.p3.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p3.setText("A");
			ialfa=ialfa+1;
		}
		
		
		holder.p4 = (TextView) convertView.findViewById(R.id.pertemuan4);
		if(p4[position]==0)
		{
			holder.p4.setText("");
		}
		else if(p4[position]==1)
		{
			holder.p4.setText("v");
			ihadir=ihadir+1;
		}
		else if(p4[position]==2)
		{
			holder.p4.setText("S");
			isakit=isakit+1;
		}
		else if(p4[position]==3)
		{
			holder.p4.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p4.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p5 = (TextView) convertView.findViewById(R.id.pertemuan5);
		if(p5[position]==0)
		{
			holder.p5.setText("");
		}
		else if(p5[position]==1)
		{
			holder.p5.setText("v");
			ihadir=ihadir+1;
		}
		else if(p5[position]==2)
		{
			holder.p5.setText("S");
			isakit=isakit+1;
		}
		else if(p5[position]==3)
		{
			holder.p5.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p5.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p6 = (TextView) convertView.findViewById(R.id.pertemuan6);
		if(p6[position]==0)
		{
			holder.p6.setText("");
		}
		else if(p6[position]==1)
		{
			holder.p6.setText("v");
			ihadir=ihadir+1;
		}
		else if(p6[position]==2)
		{
			holder.p6.setText("S");
			isakit=isakit+1;
		}
		else if(p6[position]==3)
		{
			holder.p6.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p6.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p7 = (TextView) convertView.findViewById(R.id.pertemuan7);
		if(p7[position]==0)
		{
			holder.p7.setText("");
		}
		else if(p7[position]==1)
		{
			holder.p7.setText("v");
			ihadir=ihadir+1;
		}
		else if(p7[position]==2)
		{
			holder.p7.setText("S");
			isakit=isakit+1;
		}
		else if(p7[position]==3)
		{
			holder.p7.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p7.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p8 = (TextView) convertView.findViewById(R.id.pertemuan8);
		if(p8[position]==0)
		{
			holder.p8.setText("");
		}
		else if(p8[position]==1)
		{
			holder.p8.setText("v");
			ihadir=ihadir+1;
		}
		else if(p8[position]==2)
		{
			holder.p8.setText("S");
			isakit=isakit+1;
		}
		else if(p8[position]==3)
		{
			holder.p8.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p8.setText("A");
			ialfa=ialfa+1;
			
		}
		
		holder.p9 = (TextView) convertView.findViewById(R.id.pertemuan9);
		if(p9[position]==0)
		{
			holder.p9.setText("");
		}
		else if(p9[position]==1)
		{
			holder.p9.setText("v");
			ihadir=ihadir+1;
		}
		else if(p9[position]==2)
		{
			holder.p9.setText("S");
			isakit=isakit+1;
		}
		else if(p9[position]==3)
		{
			holder.p9.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p9.setText("A");
			ialfa=ialfa+1;
			
		}
		
		holder.p10 = (TextView) convertView.findViewById(R.id.pertemuan10);
		if(p10[position]==0)
		{
			holder.p10.setText("");
		}
		else if(p10[position]==1)
		{
			holder.p10.setText("v");
			ihadir=ihadir+1;
		}
		else if(p10[position]==2)
		{
			holder.p10.setText("S");
			isakit=isakit+1;
		}
		else if(p10[position]==3)
		{
			holder.p10.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p10.setText("A");
			ialfa=ialfa+1;
			
		}
		
		holder.p11 = (TextView) convertView.findViewById(R.id.pertemuan11);
		if(p11[position]==0)
		{
			holder.p11.setText("");
		}
		else if(p11[position]==1)
		{
			holder.p11.setText("v");
			ihadir=ihadir+1;
		}
		else if(p11[position]==2)
		{
			holder.p11.setText("S");
			isakit=isakit+1;
		}
		else if(p11[position]==3)
		{
			holder.p11.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p11.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p12 = (TextView) convertView.findViewById(R.id.pertemuan12);
		if(p12[position]==0)
		{
			holder.p12.setText("");
		}
		else if(p12[position]==1)
		{
			holder.p12.setText("v");
			ihadir=ihadir+1;
		}
		else if(p12[position]==2)
		{
			holder.p12.setText("S");
			isakit=isakit+1;
		}
		else if(p12[position]==3)
		{
			holder.p12.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p12.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p13 = (TextView) convertView.findViewById(R.id.pertemuan13);
		if(p13[position]==0)
		{
			holder.p13.setText("");
		}
		else if(p13[position]==1)
		{
			holder.p13.setText("v");
			ihadir=ihadir+1;
		}
		else if(p13[position]==2)
		{
			holder.p13.setText("S");
			isakit=isakit+1;
		}
		else if(p13[position]==3)
		{
			holder.p13.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p13.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p14 = (TextView) convertView.findViewById(R.id.pertemuan14);
		if(p14[position]==0)
		{
			holder.p14.setText("");
		}
		else if(p14[position]==1)
		{
			holder.p14.setText("v");
			ihadir=ihadir+1;
		}
		else if(p14[position]==2)
		{
			holder.p14.setText("S");
			isakit=isakit+1;
		}
		else if(p14[position]==3)
		{
			holder.p14.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p14.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p15 = (TextView) convertView.findViewById(R.id.pertemuan15);
		if(p15[position]==0)
		{
			holder.p15.setText("");
		}
		else if(p15[position]==1)
		{
			holder.p15.setText("v");
			ihadir=ihadir+1;
		}
		else if(p15[position]==2)
		{
			holder.p15.setText("S");
			isakit=isakit+1;
		}
		else if(p15[position]==3)
		{
			holder.p15.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p15.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p16 = (TextView) convertView.findViewById(R.id.pertemuan16);
		if(p16[position]==0)
		{
			holder.p16.setText("");
		}
		else if(p16[position]==1)
		{
			holder.p16.setText("v");
			ihadir=ihadir+1;
		}
		else if(p16[position]==2)
		{
			holder.p16.setText("S");
			isakit=isakit+1;
		}
		else if(p16[position]==3)
		{
			holder.p16.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p16.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p17 = (TextView) convertView.findViewById(R.id.pertemuan17);
		if(p17[position]==0)
		{
			holder.p17.setText("");
		}
		else if(p17[position]==1)
		{
			holder.p17.setText("v");
			ihadir=ihadir+1;
		}
		else if(p17[position]==2)
		{
			holder.p17.setText("S");
			isakit=isakit+1;
		}
		else if(p17[position]==3)
		{
			holder.p17.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p17.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p18 = (TextView) convertView.findViewById(R.id.pertemuan18);
		if(p18[position]==0)
		{
			holder.p18.setText("");
		}
		else if(p18[position]==1)
		{
			holder.p18.setText("v");
			ihadir=ihadir+1;
		}
		else if(p18[position]==2)
		{
			holder.p18.setText("S");
			isakit=isakit+1;
		}
		else if(p18[position]==3)
		{
			holder.p18.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p18.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p19 = (TextView) convertView.findViewById(R.id.pertemuan19);
		if(p19[position]==0)
		{
			holder.p19.setText("");
		}
		else if(p19[position]==1)
		{
			holder.p19.setText("v");
			ihadir=ihadir+1;
		}
		else if(p19[position]==2)
		{
			holder.p19.setText("S");
			isakit=isakit+1;
		}
		else if(p19[position]==3)
		{
			holder.p19.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p19.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p20 = (TextView) convertView.findViewById(R.id.pertemuan20);
		if(p20[position]==0)
		{
			holder.p20.setText("");
		}
		else if(p20[position]==1)
		{
			holder.p20.setText("v");
			ihadir=ihadir+1;
		}
		else if(p20[position]==2)
		{
			holder.p20.setText("S");
			isakit=isakit+1;
		}
		else if(p20[position]==3)
		{
			holder.p20.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p20.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p21 = (TextView) convertView.findViewById(R.id.pertemuan21);
		if(p21[position]==0)
		{
			holder.p21.setText("");
		}
		else if(p21[position]==1)
		{
			holder.p21.setText("v");
			ihadir=ihadir+1;
		}
		else if(p21[position]==2)
		{
			holder.p21.setText("S");
			isakit=isakit+1;
		}
		else if(p21[position]==3)
		{
			holder.p21.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p21.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p22 = (TextView) convertView.findViewById(R.id.pertemuan22);
		if(p22[position]==0)
		{
			holder.p22.setText("");
		}
		else if(p22[position]==1)
		{
			holder.p22.setText("v");
			ihadir=ihadir+1;
		}
		else if(p22[position]==2)
		{
			holder.p22.setText("S");
			isakit=isakit+1;
		}
		else if(p22[position]==3)
		{
			holder.p22.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p22.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p23 = (TextView) convertView.findViewById(R.id.pertemuan23);
		if(p23[position]==0)
		{
			holder.p23.setText("");
		}
		else if(p23[position]==1)
		{
			holder.p23.setText("v");
			ihadir=ihadir+1;
		}
		else if(p23[position]==2)
		{
			holder.p23.setText("S");
			isakit=isakit+1;
		}
		else if(p23[position]==3)
		{
			holder.p23.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p23.setText("A");
			ialfa=ialfa+1;
		}
		
		holder.p24 = (TextView) convertView.findViewById(R.id.pertemuan24);
		if(p24[position]==0)
		{
			holder.p24.setText("");
		}
		else if(p24[position]==1)
		{
			holder.p24.setText("v");
			ihadir=ihadir+1;
		}
		else if(p24[position]==2)
		{
			holder.p24.setText("S");
			isakit=isakit+1;
		}
		else if(p24[position]==3)
		{
			holder.p24.setText("I");
			iijin=iijin+1;
		}
		else
		{
			holder.p24.setText("A");
			ialfa=ialfa+1;
		}
		
	
		holder.sakit = (TextView) convertView.findViewById(R.id.absen_sakit);
		holder.sakit.setText(""+isakit);
		holder.hadir = (TextView) convertView.findViewById(R.id.absen_hadir);
		holder.hadir.setText(""+ihadir);
		holder.ijin = (TextView) convertView.findViewById(R.id.absen_ijin);
		holder.ijin.setText(""+iijin);
		holder.alfa = (TextView) convertView.findViewById(R.id.absen_alfa);
		holder.alfa.setText(""+ialfa);
		
		float flo = (ihadir*100)/24;
		
		holder.persen = (TextView) convertView.findViewById(R.id.absen_persen);
		holder.persen.setText(""+flo+" %");
		
		
		return convertView;
	}
	

}
