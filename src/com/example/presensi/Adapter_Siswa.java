package com.example.presensi;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class Adapter_Siswa extends ArrayAdapter<String> {

	String[] nama={};
	String[] nis={};
	final Context c;
	LayoutInflater inflater;
	
	
	
	public Adapter_Siswa(Context context, String[]nama, String[]nis) {
		super(context, R.layout.model_siswa,nama);
		
		this.c = context;
		this.nama = nama;
		this.nis = nis;
	}
	
	public class ViewHolder
	{
		TextView nama;
		TextView nis;
		TextView no;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_siswa,null);
			
		}
		final ViewHolder holder = new ViewHolder();
		int x=position+1;
		holder.no = (TextView) convertView.findViewById(R.id.siswa_no);
		holder.no.setText(""+x);
		holder.nama = (TextView) convertView.findViewById(R.id.siswa_nama);
		holder.nama.setText(""+nama[position]);
		holder.nis = (TextView) convertView.findViewById(R.id.siswa_nis);
		holder.nis.setText(""+nis[position]);
		
		return convertView;
	}
	
	

}
