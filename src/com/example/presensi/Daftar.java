package com.example.presensi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Daftar extends Activity implements OnClickListener{

	EditText edit_nama,edit_nip,edit_pass1,edit_pass2;
	Button daftar,login,keluar;
	private ProgressDialog pDialog;
	String nama,nip,pass1,pass2;
	Boolean valid1=false,valid2=false,valid3=false,valid4=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_daftar);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mendaftar ...");
        pDialog.setCancelable(true);
		
		edit_nama = (EditText) findViewById(R.id.signup_nama);
		edit_nip = (EditText) findViewById(R.id.signup_nip);
		edit_pass1 = (EditText) findViewById(R.id.signup_pass);
		edit_pass2 = (EditText) findViewById(R.id.signup_pass2);
		
		daftar = (Button) findViewById(R.id.signup_tbl_signup);
		daftar.setOnClickListener(this);
		login = (Button) findViewById(R.id.signup_tbl_login);
		login.setOnClickListener(this);
		keluar = (Button) findViewById(R.id.signup_tbl_keluar);
		keluar.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.signup_tbl_signup:
			
			edit_nama.setError(null);
			edit_nip.setError(null);
			edit_pass1.setError(null);
			edit_pass2.setError(null);
			nama = edit_nama.getText().toString();
			nama = nama.replace(' ','+');
			nip = edit_nip.getText().toString();
			nip = nip.replace(' ','+');
			pass1 = edit_pass1.getText().toString();
			pass1 = pass1.replace(' ','+');
			pass2 = edit_pass2.getText().toString();
			pass2 = pass2.replace(' ','+');
			
			cek_form();
			if(valid1==true&&valid2==true&&valid3==true&&valid4==true)
			{
				daftar();
			}
			
			break;
			
		case R.id.signup_tbl_login:
			Intent login = new Intent(Daftar.this,Login.class);
			startActivity(login);
			break;
			
		case R.id.signup_tbl_keluar:
			keluar();
			break;

		
		}
	}

	
	public void keluar()
	{
		Intent keluar = new Intent(Daftar.this, MainActivity.class);
		startActivity(keluar);
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	    android.os.Process.killProcess(android.os.Process.myPid());
		finish();
		System.exit(1);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		keluar();
	}
	
	public void cek_form()
	{
		String c1 = "";
		//validasi nama
		if(nama.equals(c1))
		{
			edit_nama.setError("Nama tidak boleh kosong");
			valid1=false;
		}
		else
		{
			valid1=true;
		}
		
		//validasi nip
		if(nip.equals(c1))
		{
			edit_nip.setError("NIP tidak boleh kosong");
			valid2=false;
		}
		else
		{
			valid2=true;
		}
		
		
		if(pass1.equals(c1))
		{
			edit_pass1.setError("Password tidak boleh kosong");
			valid3=false;
		}
		else if(pass1.length()<8)
		{
			edit_pass1.setError("Password minimal 8 karakter");
			valid3=false;
		}
		else
		{
			valid3=true;
		}
		
		
		//validasi pass2
		if(pass2.equals(c1))
		{
			edit_pass2.setError("Password tidak boleh kosong");
			valid4=false;
		}
		else
		{
			if(pass2.equals(pass1))
			{
				valid4=true;
			}
			else
			{
				edit_pass2.setError("Password harus sama");
				valid4=false;
			}
		}
		
	}
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
	public void daftar()
	{
		String url = "http://tolong.hol.es/penilaian/daftar.php?nama="+nama+"&nip="+nip+"&pass="+pass1;
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        
                        try {
                                JSONObject login = (JSONObject) response
                                        .get(0);
                                
                                String peringatan = login.getString("validasi");
                                String cek1="0";
                                String cek2="1";
                                String cek3="2";
                                
                                if(peringatan.equals(cek1))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "NIP sudah terdaftar", Toast.LENGTH_SHORT).show();
                                }
                                if(peringatan.equals(cek2))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Registrasi Gagal", Toast.LENGTH_SHORT).show(); 
                                }
                                if(peringatan.equals(cek3))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Registrasi Berhasil, silakan login untuk melanjutkan", Toast.LENGTH_LONG).show(); 
                                	
                                	Intent intent = new Intent(Daftar.this,Login.class);
                                	startActivity(intent);		
                                }
                                
                        
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                            		"Pendaftaran gagal, cek koneksi internet anda",
                                    Toast.LENGTH_LONG).show();
                            hidepDialog();
                        }
 
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                        		"Pendaftaran gagal, cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
        showpDialog();
        
        
    }

}
