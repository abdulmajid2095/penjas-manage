package com.example.presensi;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_penilaian extends ArrayAdapter<String> {

	String[] nama={};
	String[] nis={};
	int[] n1={};
	int[] n2={};
	int[] n3={};
	int[] n4={};
	int[] n5={};
	int[] n6={};
	String kkm;
	Context c;
	LayoutInflater inflater;
	
	
	
	public Adapter_penilaian(Context context, String[]nama, String[]nis, int[]n1, int[]n2, int[]n3, int[]n4, int[]n5, int[]n6, String kkm) {
		super(context, R.layout.model_penilaian,nama);
		
		this.c = context;
		this.nama = nama;
		this.nis = nis;
		this.n1=n1;
		this.n2=n2;
		this.n3=n3;
		this.n4=n4;
		this.n5=n5;
		this.n6=n6;
		this.kkm=kkm;
	}
	
	public class ViewHolder
	{
		TextView nama,n1,n2,n3,n4,n5,n6,rata,kkm,keterangan;
		TextView nis;
		TextView no;
		
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_penilaian,null);
			
		}
		final ViewHolder holder = new ViewHolder();
		int x=position+1;
		
		int kkmbaru = Integer.parseInt(kkm);
		
		holder.no = (TextView) convertView.findViewById(R.id.siswa_no);
		holder.no.setText(""+x);
		holder.nama = (TextView) convertView.findViewById(R.id.siswa_nama);
		holder.nama.setText(""+nama[position]);
		holder.nis = (TextView) convertView.findViewById(R.id.siswa_nis);
		holder.nis.setText(""+nis[position]);
		holder.kkm = (TextView) convertView.findViewById(R.id.siswa_kkm);
		holder.kkm.setText(""+kkmbaru);
		holder.n1 = (TextView) convertView.findViewById(R.id.siswa_n1);
		holder.n1.setText(""+n1[position]);
		if(n1[position]<kkmbaru)
		{
			holder.n1.setTextColor(Color.RED);
		}
		holder.n2 = (TextView) convertView.findViewById(R.id.siswa_n2);
		holder.n2.setText(""+n2[position]);
		if(n2[position]<kkmbaru)
		{
			holder.n2.setTextColor(Color.RED);
		}
		holder.n3 = (TextView) convertView.findViewById(R.id.siswa_n3);
		holder.n3.setText(""+n3[position]);
		if(n3[position]<kkmbaru)
		{
			holder.n3.setTextColor(Color.RED);
		}
		holder.n4 = (TextView) convertView.findViewById(R.id.siswa_n4);
		holder.n4.setText(""+n4[position]);
		if(n4[position]<kkmbaru)
		{
			holder.n4.setTextColor(Color.RED);
		}
		holder.n5 = (TextView) convertView.findViewById(R.id.siswa_n5);
		holder.n5.setText(""+n5[position]);
		if(n5[position]<kkmbaru)
		{
			holder.n5.setTextColor(Color.RED);
		}
		holder.n6 = (TextView) convertView.findViewById(R.id.siswa_n6);
		holder.n6.setText(""+n6[position]);
		if(n6[position]<kkmbaru)
		{
			holder.n6.setTextColor(Color.RED);
		}
		
		int rata2=(n1[position]+n2[position]+n3[position]+n4[position]+n5[position]+n6[position])/6;
		holder.rata = (TextView) convertView.findViewById(R.id.siswa_rata);
		holder.rata.setText(""+rata2);
		holder.keterangan = (TextView) convertView.findViewById(R.id.siswa_keterangan);
		if(rata2<kkmbaru)
		{
			holder.keterangan.setText("Remidi");
			holder.keterangan.setTextColor(Color.RED);
		}
		else
		{
			holder.keterangan.setText("Lulus");
		}
		
	
		
		return convertView;
	}
	

}
