package com.example.presensi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_Kelas extends ArrayAdapter<String>{

	String[] nama={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Kelas(Context context, String[]nama) {
		super(context, R.layout.model_kelas,nama);
		
		this.c = context;
		this.nama = nama;
	}
	
	public class ViewHolder
	{
		TextView nama;
		
	}
	
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_kelas,null);
		}
		
		
		final ViewHolder holder = new ViewHolder();
		holder.nama = (TextView) convertView.findViewById(R.id.nama_kelas);
		holder.nama.setText(""+nama[position]);
		
		return convertView;
	}

}
