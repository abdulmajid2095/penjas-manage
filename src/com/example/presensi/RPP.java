package com.example.presensi;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.ipaulpro.afilechooser.utils.FileUtils;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RPP extends Activity implements OnClickListener{

	private static final int REQUEST_CODE = 6384;
	Button upload,pilih; 
	TextView teks_lokasi;
	String nama_files;
	EditText edit_nama;
	Boolean valid;
	int serverResponseCode = 0;
	String path;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_rpp);
		
		upload = (Button) findViewById(R.id.rpp_upload);
		upload.setOnClickListener(this);
		pilih = (Button) findViewById(R.id.rpp_pilih);
		pilih.setOnClickListener(this);
		
		teks_lokasi = (TextView)findViewById(R.id.teks_path);
		edit_nama = (EditText) findViewById(R.id.rpp_edittext);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rpp_pilih:
			showChooser();
			break;
		case R.id.rpp_upload:
			edit_nama.setError(null);
			nama_files = edit_nama.getText().toString();
			cek();
			if(valid==true)
			{
				
				if (path != null && FileUtils.isLocal(path)) {
				    File file = new File(path);
				   }

				   

				   new Thread(new Runnable() {
				    public void run() {
				     runOnUiThread(new Runnable() {
				      public void run() {
				       
				      }
				     });

				     int response = 0;

				     String judul = edit_nama.getText().toString();
				     response = uploadFile(path, judul);
				     System.out.println("RES : " + response);

				    }
				   }).start();
				
			}
			
			break;

		}
	}
	
	public void cek()
	{
		String zx = "";
		if(nama_files.equals(zx))
		{
			edit_nama.setError("Nama file tidak boleh kosong");
			valid=false;
		}
		else
		{
			valid=true;
		}
		
	}
	

	private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        Intent target = FileUtils.createGetContentIntent();
        // Create the chooser Intent
        Intent intent = Intent.createChooser(
                target, getString(R.string.chooser_title));
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                // If the file selection was successful
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        
                        try {
                            // Get the file path from the URI
                            path = FileUtils.getPath(this, uri);
                            /*Toast.makeText(RPP.this,
                                    "File Selected: " + path, Toast.LENGTH_LONG).show();*/
                            teks_lokasi.setText("Lokasi File : "+path);
                        } catch (Exception e) {
                            Log.e("FileSelectorTestActivity", "File select error", e);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    
    public int uploadFile(String sourceFileUri, String judul) {

    	  // ip komputer server
    	  String upLoadServerUri = "http://192.168.43.26/new/upload.php";
    	  String fileName = sourceFileUri;

    	  HttpURLConnection conn = null;
    	  DataOutputStream dos = null;
    	  String lineEnd = "\r\n";
    	  String twoHyphens = "--";
    	  String boundary = "*****";
    	  int bytesRead, bytesAvailable, bufferSize;
    	  byte[] buffer;
    	  int maxBufferSize = 1 * 1024 * 1024;
    	  File sourceFile = new File(sourceFileUri);
    	  if (!sourceFile.isFile()) {
    	   Log.e("uploadFile", "Source File Does not exist");
    	   return 0;
    	  }
    	  try {
    	   FileInputStream fileInputStream = new FileInputStream(sourceFile);
    	   URL url = new URL(upLoadServerUri);
    	   conn = (HttpURLConnection) url.openConnection();
    	   conn.setDoInput(true); // Allow Inputs
    	   conn.setDoOutput(true); // Allow Outputs
    	   conn.setUseCaches(false); // Don't use a Cached Copy
    	   conn.setRequestMethod("POST");
    	   conn.setRequestProperty("Connection", "Keep-Alive");
    	   conn.setRequestProperty("Content-Type",
    	     "multipart/form-data;boundary=" + boundary);
    	   conn.setRequestProperty("uploaded_file", fileName);
    	   dos = new DataOutputStream(conn.getOutputStream());
    	   dos.writeBytes(twoHyphens + boundary + lineEnd);

    	   // untuk parameter keterangan
    	   dos.writeBytes("Content-Disposition: form-data; name=\"judul\""
    	     + lineEnd);
    	   dos.writeBytes(lineEnd);
    	   dos.writeBytes(judul);
    	   dos.writeBytes(lineEnd);
    	   dos.writeBytes(twoHyphens + boundary + lineEnd);

    	   // jika ingin menambahkan parameter baru, silahkan buat baris baru
    	   // lagi seperti berikut
    	   // dos.writeBytes("Content-Disposition: form-data; name=\"keterangan\""+
    	   // lineEnd);
    	   // dos.writeBytes(lineEnd);
    	   // dos.writeBytes(keterangan);
    	   // dos.writeBytes(lineEnd);
    	   // dos.writeBytes(twoHyphens + boundary + lineEnd);

    	   dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
    	     + fileName + "\"" + lineEnd);
    	   dos.writeBytes(lineEnd);
    	   // create a buffer of maximum size
    	   bytesAvailable = fileInputStream.available();
    	   bufferSize = Math.min(bytesAvailable, maxBufferSize);
    	   buffer = new byte[bufferSize];
    	   // read file and write it into form...
    	   bytesRead = fileInputStream.read(buffer, 0, bufferSize);

    	   while (bytesRead > 0) {
    	    dos.write(buffer, 0, bufferSize);
    	    bytesAvailable = fileInputStream.available();
    	    bufferSize = Math.min(bytesAvailable, maxBufferSize);
    	    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
    	   }

    	   dos.writeBytes(lineEnd);
    	   dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

    	   serverResponseCode = conn.getResponseCode();
    	   String serverResponseMessage = conn.getResponseMessage();

    	   if (serverResponseCode == 200) {
    	    runOnUiThread(new Runnable() {
    	     public void run() {
    	      //textViewKeterangan.setText("Upload Berhasil");
    	      Toast.makeText(RPP.this, "Upload Berhasil.",
    	        Toast.LENGTH_LONG).show();
    	     }
    	    });
    	   }

    	   // close the streams //
    	   fileInputStream.close();
    	   dos.flush();
    	   dos.close();

    	  } catch (MalformedURLException ex) {
    	   
    	   ex.printStackTrace();
    	   Toast.makeText(RPP.this, "MalformedURLException",
    	     Toast.LENGTH_SHORT).show();
    	   Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
    	  } catch (Exception e) {
    	   
    	   e.printStackTrace();
    	   Toast.makeText(RPP.this, "Exception : " + e.getMessage(),
    	     Toast.LENGTH_SHORT).show();
    	   Log.e("Upload file to server Exception",
    	     "Exception : " + e.getMessage(), e);
    	  }
    	 
    	  return serverResponseCode;

    	 }
    
}
