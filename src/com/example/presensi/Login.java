package com.example.presensi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener{

	EditText edit_nip,edit_pass;
	Button login,daftar,keluar;
	private ProgressDialog pDialog;
	String nip,pass;
	Boolean valid1=false,valid2=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_login);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Logging In");
        pDialog.setCancelable(false);
        
		login = (Button)findViewById(R.id.login_tbl_login);
		login.setOnClickListener(this);
		daftar = (Button)findViewById(R.id.login_tbl_signup);
		daftar.setOnClickListener(this);
		keluar = (Button)findViewById(R.id.login_tbl_keluar);
		keluar.setOnClickListener(this);
		
		edit_nip = (EditText) findViewById(R.id.login_nip);
		edit_pass = (EditText) findViewById(R.id.login_pass);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.login_tbl_login:
		
			edit_nip.setError(null);
			edit_pass.setError(null);
			nip = edit_nip.getText().toString();
			pass = edit_pass.getText().toString();
			cek_editText();
			if(valid1==true&&valid2==true)
			{
				cek_login();
			}
			break;
			
		case R.id.login_tbl_signup:
			Intent daftar = new Intent(Login.this,Daftar.class);
			startActivity(daftar);
			break;
			
		case R.id.login_tbl_keluar:
			keluar();
			break;

		
		}
	}
	
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    public void cek_editText()
	{
		String c1 = "";
		
		if(nip.equals(c1))
		{
			edit_nip.setError("NIP tidak boleh kosong");
			valid1=false;
		}
		else
		{
			valid1=true;
		}
		
			
		if(pass.equals(c1))
		{
			edit_pass.setError("Password tidak boleh kosong");
			valid2=false;
		}
		else if(pass.length()<8)
		{
			edit_pass.setError("Password minimal 8 karakter");
			valid2=false;
		}
		else
		{
			valid2=true;
		}
	}
    
    public void cek_login()
	{
		String url = "http://tolong.hol.es/penilaian/login.php?nip="+nip+"&password="+pass;
		
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                                JSONObject login = (JSONObject) response
                                        .get(0);
                                
                                String peringatan = login.getString("validasi");
                                String cek1="0";
                                String cek2="1";
                                String cek3="2";
                                
                                if(peringatan.equals(cek1))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Akun belum terdaftar", Toast.LENGTH_SHORT).show();
                                }
                                if(peringatan.equals(cek2))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Login gagal. Cek password anda", Toast.LENGTH_SHORT).show(); 
                                }
                                if(peringatan.equals(cek3))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Login berhasil", Toast.LENGTH_SHORT).show(); 
                                	
                                	Dataku data = Dataku.findById(Dataku.class, 1L);
                                	data.idku = login.getString("idku");
                                 	data.nama = login.getString("nama");
                                 	data.nip = login.getString("nip");
                                 	data.pass = login.getString("pass");
                                 	data.login = 1;
                                 	data.save();
                                 	
                                 	Intent intent = new Intent(Login.this,Menuku.class);
                                 	startActivity(intent);
                                
                                }
                        
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                            		"Login gagal, cek koneksi internet anda",
                                    Toast.LENGTH_LONG).show();
                            hidepDialog();
                        }
 
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        
                        Toast.makeText(getApplicationContext(),
                                "Login gagal, cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
        showpDialog();
        
        
    }

    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	keluar();
    }
    
    public void keluar()
    {
    	Intent keluar = new Intent(Login.this, MainActivity.class);
		startActivity(keluar);
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	    android.os.Process.killProcess(android.os.Process.myPid());
		finish();
		System.exit(1);
    }
	
}
