package com.example.presensi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.presensi.R.id;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.renderscript.Type;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Penilaian_tampil extends Activity implements OnRefreshListener, OnClickListener{

	private SwipeRefreshLayout swipeRefreshLayout;
	ListView hasil_siswa;
	Dataku data;
	String[] id_siswa;
	String[] nama_siswa;
	String[] nis;
	String[] nilai_baru;
	int[] n1,n2,n3,n4,n5,n6;
	EditText[] edit_nilai;
	String id_kelas,nama_kelas,tahun,kkm;
	TextView namakelas,tahunku;
	Button tambah;
	private ProgressDialog pDialog;
	int nilaike, jumlah_data;
	TextView kosong;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_penilaian_tampil);
		
		Intent kelas = getIntent();
		nama_kelas = kelas.getStringExtra("nama_kelas");
		id_kelas = kelas.getStringExtra("id_kelas");
		tahun = kelas.getStringExtra("tahun");
		kkm = kelas.getStringExtra("kkm");
		
		namakelas = (TextView)findViewById(R.id.siswa_nama_kelas);
		namakelas.setText("Nama Kelas : "+nama_kelas);
		tahunku = (TextView)findViewById(R.id.siswa_tahun);
		tahunku.setText(""+tahun);
		
		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.siswa_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		hasil_siswa = (ListView)findViewById(R.id.hasil_siswa);
		
		tambah = (Button) findViewById(R.id.tbl_tambah);
        tambah.setOnClickListener(this);
        
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mengganti Nilai ...");
        pDialog.setCancelable(true);
        kosong = (TextView) findViewById(R.id.teks_kosong);
        ambil_data();
	}

	
	
	
private void ambil_data() {
    	
    	String json_alamat = "http://tolong.hol.es/penilaian/daftar_siswa.php?id_kelas="+id_kelas;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        try {
                        	
                        	jumlah_data = response.length();
                        	if(jumlah_data==0)
                        	{
                        		kosong.setVisibility(View.VISIBLE);
                        	}
                        	else
                        	{
                        		kosong.setVisibility(View.GONE);
                        	}
                        	id_siswa = new String[jumlah_data];
                        	nama_siswa = new String[jumlah_data];
                        	nis = new String[jumlah_data];
                        	n1 = new int[jumlah_data];
                        	n2 = new int[jumlah_data];
                        	n3 = new int[jumlah_data];
                        	n4 = new int[jumlah_data];
                        	n5 = new int[jumlah_data];
                        	n6 = new int[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	id_siswa[i] = hasil.getString("id_siswa");
                                    nama_siswa[i] = hasil.getString("nama");
                                    nis[i] = hasil.getString("nis");
                                    n1[i] = Integer.parseInt(hasil.getString("n1"));
                                    n2[i] = Integer.parseInt(hasil.getString("n2"));
                                    n3[i] = Integer.parseInt(hasil.getString("n3"));
                                    n4[i] = Integer.parseInt(hasil.getString("n4"));
                                    n5[i] = Integer.parseInt(hasil.getString("n5"));
                                    n6[i] = Integer.parseInt(hasil.getString("n6"));
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Hasil tidak ditemukan",
                                    Toast.LENGTH_LONG).show();
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_penilaian adapter = new Adapter_penilaian(getApplicationContext(), nama_siswa, nis, n1, n2, n3, n4, n5, n6, kkm);
                		hasil_siswa.setAdapter(adapter);
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }




@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	ambil_data();
}




@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.tbl_tambah:
		dialog_tambah();
		break;
}}




private void dialog_tambah() {

    // custom dialog
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_pilih_nilai);
    Button nilai1 = (Button) dialog.findViewById(R.id.dialog_penilaian1);
    nilai1.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=1;
			dialog.dismiss();
			dialog_edit();
		}
	});
    Button nilai2 = (Button) dialog.findViewById(R.id.dialog_penilaian2);
    nilai2.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=2;
			dialog.dismiss();
			dialog_edit();
		}
	});
    Button nilai3 = (Button) dialog.findViewById(R.id.dialog_penilaian3);
    nilai3.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=3;
			dialog.dismiss();
			dialog_edit();
		}
	});
    Button nilai4 = (Button) dialog.findViewById(R.id.dialog_penilaian4);
    nilai4.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=4;
			dialog.dismiss();
			dialog_edit();
		}
	});
    Button nilai5 = (Button) dialog.findViewById(R.id.dialog_penilaian5);
    nilai5.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=5;
			dialog.dismiss();
			dialog_edit();
		}
	});
    Button nilai6 = (Button) dialog.findViewById(R.id.dialog_penilaian6);
    nilai6.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nilaike=6;
			dialog.dismiss();
			dialog_edit();
		}
	});
    
    dialog.show();

}


public void dialog_edit()
{
	// custom dialog
	final Dialog dialog = new Dialog(this);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.model_dialog_edit_nilai);
	TextView teks = (TextView) dialog.findViewById(R.id.nilaike);
	teks.setText("Nilai "+nilaike);
	
	nilai_baru = new String[jumlah_data];
	edit_nilai = new EditText[jumlah_data];
	
	for(int i=0;i<jumlah_data;i++)
	{
	
	
	LinearLayout layout1 = (LinearLayout) dialog.findViewById(R.id.layout);
	
	//Layout Nama dan Nilai
	LinearLayout layout2 = new LinearLayout(this);
	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	layout2.setLayoutParams(params2);
	layout2.setOrientation(LinearLayout.HORIZONTAL);
	
	
	//Layout Nama
	LinearLayout layout3 = new LinearLayout(this);
	LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT);
	params3.weight = 1.0f;
	params3.gravity = Gravity.CENTER_VERTICAL;
	layout3.setLayoutParams(params3);
	
	
	TextView nama = new TextView(this);
	nama.setText(""+nama_siswa[i]);
	nama.setTextSize(16);
	LinearLayout.LayoutParams paramsx = new LinearLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT );
	paramsx.weight = 1.0f;
	nama.setLayoutParams(paramsx);
	
	//Layout Nilai
	LinearLayout layout4 = new LinearLayout(this);
	LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(130, LayoutParams.WRAP_CONTENT);
	params4.leftMargin=30;
	params4.rightMargin=20;
	layout4.setLayoutParams(params4);
	
	
	//final EditText nilai = new EditText(this);
	edit_nilai[i] = new EditText(this);
	edit_nilai[i].setTextSize(18);
	edit_nilai[i].setInputType(InputType.TYPE_CLASS_NUMBER);
	
	if(nilaike==1)
	{
		if(n1[i]!=0)
		{
			edit_nilai[i].setText(""+n1[i]);
		}
	}
	else if(nilaike==2)
	{
		if(n2[i]!=0)
		{
			edit_nilai[i].setText(""+n2[i]);
		}
	}
	else if(nilaike==3)
	{
		if(n3[i]!=0)
		{
			edit_nilai[i].setText(""+n3[i]);
		}
	}
	else if(nilaike==4)
	{
		if(n4[i]!=0)
		{
			edit_nilai[i].setText(""+n4[i]);
		}
	}
	else if(nilaike==5)
	{
		if(n5[i]!=0)
		{
			edit_nilai[i].setText(""+n5[i]);
		}
	}
	else
	{
		if(n6[i]!=0)
		{
			edit_nilai[i].setText(""+n6[i]);
		}
	}
	
	LinearLayout.LayoutParams paramsz = new LinearLayout.LayoutParams(150,LayoutParams.WRAP_CONTENT );
	paramsz.gravity = Gravity.CENTER;
	edit_nilai[i].setLayoutParams(paramsz);

	LinearLayout layout5 = new LinearLayout(this);
	LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 2);
	params5.topMargin=10;
	params5.bottomMargin=10;
	layout5.setLayoutParams(params5);
	layout5.setBackgroundColor(Color.BLACK);
		
	
	layout1.addView(layout2);
	layout2.addView(layout3);
	layout2.addView(layout4);
	layout3.addView(nama);
	layout4.addView(edit_nilai[i]);
	layout1.addView(layout5);
	
	if(i==jumlah_data-1)
	{
		Button btn = (Button) dialog.findViewById(R.id.dialog_nilai_simpan);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				for(int m=0;m<jumlah_data;m++)
				{
					String d="";
					nilai_baru[m] = edit_nilai[m].getText().toString();
					if(nilai_baru[m].equals(d))
					{
						nilai_baru[m]="0";
					}
					nilai_baru[m] = nilai_baru[m].replace(' ','+');
				}
				
				edit_nilai();
				dialog.dismiss();
				
			}
		});
	}
	
	
	}
	
	
	dialog.show(); 
}

public void edit_nilai()
{
	String url = "http://tolong.hol.es/penilaian/update_nilai.php?jumlahdata="+jumlah_data+"&nilaike="+nilaike;
	
	for(int z=0;z<jumlah_data;z++)
	{
		url = url+"&id_siswa"+z+"="+id_siswa[z]+"&nilai"+z+"="+nilai_baru[z];
	}
	//Toast.makeText(getApplicationContext(), ""+url, Toast.LENGTH_LONG).show();
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="1";
                            String cek2="2";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penggantian Nilai Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penggantian Nilai Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Pendaftaran gagal, cek koneksi internet anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Pendaftaran gagal, cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
    
    
}

private void showpDialog() {
    if (!pDialog.isShowing())
        pDialog.show();
}

private void hidepDialog() {
    if (pDialog.isShowing())
        pDialog.dismiss();
}

	
}
