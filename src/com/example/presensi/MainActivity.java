package com.example.presensi;


import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        
        Dataku data = new Dataku("","","","",0);
        data.save();
        
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				
				Dataku data2 = Dataku.findById(Dataku.class, 1L);
				if(data2.login==1)
				{
					Intent lanjut = new Intent(MainActivity.this, Menuku.class);
	    			startActivity(lanjut);
				}
				else
				{
					Intent lanjut = new Intent(MainActivity.this, Login.class);
	    			startActivity(lanjut);
				}
			}
		},2000);
    }


    
}
