package com.example.presensi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Daftar_Siswa extends Activity implements OnRefreshListener, OnClickListener, OnItemLongClickListener{

	private SwipeRefreshLayout swipeRefreshLayout;
	ListView hasil_siswa;
	Dataku data;
	String[] id_siswa;
	String[] nama_siswa;
	String[] nis;
	String id_kelas,nama_kelas,siswa_baru,nis_baru;
	TextView namakelas;
	Button tambah;
	private ProgressDialog pDialog;
	TextView kosong;
	int dipilih;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_daftar__siswa);
		
		Intent kelas = getIntent();
		nama_kelas = kelas.getStringExtra("nama_kelas");
		id_kelas = kelas.getStringExtra("id_kelas");
		namakelas = (TextView)findViewById(R.id.siswa_nama_kelas);
		namakelas.setText("Nama Kelas : "+nama_kelas);
		
		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.siswa_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		hasil_siswa = (ListView)findViewById(R.id.hasil_siswa);
		hasil_siswa.setOnItemLongClickListener(this);
		
		tambah = (Button) findViewById(R.id.tbl_tambah);
        tambah.setOnClickListener(this);
        
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Tunggu ...");
        pDialog.setCancelable(true);
        
        kosong = (TextView) findViewById(R.id.teks_kosong);
               
        ambil_data();
	}

	
	
	
private void ambil_data() {
    	
    	String json_alamat = "http://tolong.hol.es/penilaian/daftar_siswa.php?id_kelas="+id_kelas;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        try {
                        	
                        	int jumlah_data = response.length();
                        	if(jumlah_data==0)
                        	{
                        		kosong.setVisibility(View.VISIBLE);
                        	}
                        	else
                        	{
                        		kosong.setVisibility(View.GONE);
                        	}
                            
                        	id_siswa = new String[jumlah_data];
                        	nama_siswa = new String[jumlah_data];
                        	nis = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	id_siswa[i] = hasil.getString("id_siswa");
                                    nama_siswa[i] = hasil.getString("nama");
                                    nis[i] = hasil.getString("nis");
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Hasil tidak ditemukan",
                                    Toast.LENGTH_LONG).show();
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_Siswa adapter = new Adapter_Siswa(getApplicationContext(), nama_siswa, nis);
                		hasil_siswa.setAdapter(adapter);
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }




@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	ambil_data();
}




@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.tbl_tambah:
		dialog_tambah();
		break;
}}




private void dialog_tambah() {

    // custom dialog
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_tambah_siswa);
    final EditText namaku = (EditText) dialog.findViewById(R.id.dialog_tambah_siswa_edittext1);
    final EditText nisku = (EditText) dialog.findViewById(R.id.dialog_tambah_siswa_edittext2);
    Button simpan = (Button) dialog.findViewById(R.id.dialog_tambah_siswa_simpan);
    simpan.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			siswa_baru = namaku.getText().toString();
			siswa_baru = siswa_baru.replace(' ','+');
			nis_baru = nisku.getText().toString();
			nis_baru = nis_baru.replace(' ','+');
			tambah_siswa();
			showpDialog();
			dialog.dismiss();	
		}
	});
    dialog.show();

}


public void tambah_siswa()
{
	String url = "http://tolong.hol.es/penilaian/tambah_siswa.php?nama="+siswa_baru+"&nis="+nis_baru+"&id_kelas="+id_kelas;
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="1";
                            String cek2="2";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penambahan Siswa Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penambahan Siswa Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Pendaftaran gagal, cek koneksi internet anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Pendaftaran gagal, cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
    
    
}

private void showpDialog() {
    if (!pDialog.isShowing())
        pDialog.show();
}

private void hidepDialog() {
    if (pDialog.isShowing())
        pDialog.dismiss();
}


@Override
public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int posisi,
		long arg3) {
	// TODO Auto-generated method stub
	dipilih=posisi;
	pilihan();
	return true;
}


	
public void pilihan()
{
	final Dialog dialog = new Dialog(this);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.model_dialog_pilihan);
	dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	Button edit = (Button) dialog.findViewById(R.id.edit);
	edit.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			edit();
		}
	});
	Button hapus = (Button) dialog.findViewById(R.id.hapus);
	hapus.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus();
		}
	});
	
	dialog.show();
	}
	
	
public void hapus()
{
	final Dialog dialog = new Dialog(this);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.model_dialog_hapus);
	dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	TextView teks = (TextView) dialog.findViewById(R.id.hapus_teks);
    teks.setText("Hapus "+nama_siswa[dipilih]+" dari daftar siswa ?");
	Button ya = (Button) dialog.findViewById(R.id.ya);
	ya.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			hapus_siswa();
		}
	});
	Button tidak = (Button) dialog.findViewById(R.id.tidak);
	tidak.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialog.dismiss();
		}
	});
	
	dialog.show();
}
	
public void edit()
{
	final Dialog dialog = new Dialog(this);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.model_dialog_edit_siswa);
	final EditText tx1 = (EditText) dialog.findViewById(R.id.edittext);
	tx1.setText(""+nis[dipilih]);
	final EditText tx2 = (EditText) dialog.findViewById(R.id.edittext2);
	tx2.setText(""+nama_siswa[dipilih]);
	
	Button spn = (Button) dialog.findViewById(R.id.simpan);
	spn.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			nis_baru = tx1.getText().toString();
			nis_baru = nis_baru.replace(' ','+');
			siswa_baru = tx2.getText().toString();
			siswa_baru = siswa_baru.replace(' ','+');
			dialog.dismiss();
			simpan_edit();
		}
	});
	
	
	dialog.show();
}
	
	
public void simpan_edit()
{
	String url = "http://tolong.hol.es/penilaian/update_siswa.php?nama="+siswa_baru+"&nis="+nis_baru+"&id="+id_siswa[dipilih];
	JsonArrayRequest req = new JsonArrayRequest(url,
	        new Response.Listener<JSONArray>() {
	            @Override
	            public void onResponse(JSONArray response) {
	                
	                try {
	                        JSONObject login = (JSONObject) response
	                                .get(0);
	                        
	                        String peringatan = login.getString("validasi");
	                        String cek1="0";
	                        String cek2="1";
	                        
	                        if(peringatan.equals(cek1))
	                        {
	                        	hidepDialog();
	                        	Toast.makeText(getApplicationContext(), "Update Siswa Gagal", Toast.LENGTH_SHORT).show(); 
	                        }
	                        if(peringatan.equals(cek2))
	                        {
	                        	hidepDialog();
	                        	Toast.makeText(getApplicationContext(), "Update Siswa Berhasil", Toast.LENGTH_LONG).show(); 
	                        	onRefresh();
	                        		
	                        }
	                        
	                
	                        
	                } catch (JSONException e) {
	                    e.printStackTrace();
	                    Toast.makeText(getApplicationContext(),
	                    		"Periksa sambungan anda",
	                            Toast.LENGTH_LONG).show();
	                    hidepDialog();
	                }
	
	                
	            }
	        }, new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	                Toast.makeText(getApplicationContext(),
	                		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
	                hidepDialog();
	            }
	        });
	// Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
	requestQueue.add(req);
	showpDialog();
}
	
public void hapus_siswa()
{
	String url = "http://tolong.hol.es/penilaian/hapus_siswa.php?id="+id_siswa[dipilih];
	JsonArrayRequest req = new JsonArrayRequest(url,
	        new Response.Listener<JSONArray>() {
	            @Override
	            public void onResponse(JSONArray response) {
	                
	                try {
	                        JSONObject login = (JSONObject) response
	                                .get(0);
	                        
	                        String peringatan = login.getString("validasi");
	                        String cek1="0";
	                        String cek2="1";
	                        
	                        if(peringatan.equals(cek1))
	                        {
	                        	hidepDialog();
	                        	Toast.makeText(getApplicationContext(), "Hapus Siswa Gagal", Toast.LENGTH_SHORT).show(); 
	                        }
	                        if(peringatan.equals(cek2))
	                        {
	                        	hidepDialog();
	                        	Toast.makeText(getApplicationContext(), "Hapus Siswa Berhasil", Toast.LENGTH_LONG).show(); 
	                        	onRefresh();
	                        		
	                        }
	                        
	                
	                        
	                } catch (JSONException e) {
	                    e.printStackTrace();
	                    Toast.makeText(getApplicationContext(),
	                    		"Periksa sambungan anda",
	                            Toast.LENGTH_LONG).show();
	                    hidepDialog();
	                }
	
	                
	            }
	        }, new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	                Toast.makeText(getApplicationContext(),
	                		"Periksa sambungan anda", Toast.LENGTH_SHORT).show();
	                hidepDialog();
	            }
	        });
	// Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
	requestQueue.add(req);
	showpDialog();
}


}
