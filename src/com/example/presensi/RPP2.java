package com.example.presensi;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;

public class RPP2 extends Activity implements OnClickListener {

 TextView textViewPath, textViewKeterangan;
 EditText editTextJudul;
 Button buttonPilihFile, buttonUpload;
 String id_guru="";
 Dataku data;
 String path;
 int ukuran_file;
 
 Boolean size_besar=false,teks_kosong=false, file_dipilih=false;
 int serverResponseCode = 0;
 ProgressDialog pDialog = null;
 
 @Override
 protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  requestWindowFeature(Window.FEATURE_NO_TITLE);
  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
  setContentView(R.layout.activity_rpp2);

  data = Dataku.findById(Dataku.class, 1L);
  id_guru = ""+data.idku;
  init();
 }

 protected void init() {
  textViewPath = (TextView) findViewById(R.id.textViewPath);
  //textViewPath.setText("");

  editTextJudul = (EditText) findViewById(R.id.editTextJudul);
  //editTextJudul.setText("");

  textViewKeterangan = (TextView) findViewById(R.id.textViewKeterangan);
  //textViewKeterangan.setText("");

  buttonPilihFile = (Button) findViewById(R.id.buttonPilihFile);
  buttonPilihFile.setOnClickListener(this);
  buttonUpload = (Button) findViewById(R.id.buttonUpload);
  buttonUpload.setOnClickListener(this);
  
  pDialog = new ProgressDialog(this);
  pDialog.setMessage("Upload file, silakan tunggu ...");
  pDialog.setCancelable(false);
 }

 private static final int REQUEST_CHOOSER = 1234;

 @Override
 public void onClick(View v) {
  switch (v.getId()) {
  case R.id.buttonPilihFile:

   Intent getContentIntent = FileUtils.createGetContentIntent();
   Intent intent = Intent
     .createChooser(getContentIntent, "Pilih file");
   startActivityForResult(intent, REQUEST_CHOOSER);

   break;
  case R.id.buttonUpload:

	  if(editTextJudul.getText().toString().equals(""))
	  {
		  teks_kosong=true;
		  Toast.makeText(getApplicationContext(), "Pilih file dan Isi Judul terlebih dahulu", Toast.LENGTH_SHORT).show();
	  }
	  else
	  {
		  if (path != null && FileUtils.isLocal(path)&& size_besar==false && file_dipilih==true) {
			    File file = new File(path);
			   }

			   showpDialog();

			   new Thread(new Runnable() {
			    public void run() {
			     runOnUiThread(new Runnable() {
			      public void run() {
			       textViewKeterangan.setText("Keterangan : Proses Upload");
			      }
			     });

			     int response = 0;

			     String judul = editTextJudul.getText().toString();
			     response = uploadFile(path, judul);
			     System.out.println("RES : " + response);

			    }
			   }).start();
	  }
  
   

   break;
  }
 }

 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  switch (requestCode) {

  case REQUEST_CHOOSER:
   if (resultCode == RESULT_OK) {

    final Uri uri = data.getData();
    path = FileUtils.getPath(this, uri);
    java.net.URI uri2;
	try {
		file_dipilih=true;
		uri2 = new java.net.URI(uri.toString());
		File f = new File(uri2.getPath());
	    long size = f.length();
	    ukuran_file = (int) size;
	    if(ukuran_file>1048576)
	    {
	    	size_besar=true;
	    	Toast.makeText(getApplicationContext(), "Pilih file dengan maksimal ukuran 1MB", Toast.LENGTH_LONG).show();
	    }
	    else
	    {
	    	size_besar=false;
	    }
	    //Toast.makeText(getApplicationContext(), "Size = "+size, Toast.LENGTH_SHORT).show();
	} catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    textViewPath.setText("Lokasi File : "+path);
   }

   break;
  }
 }

 

 public int uploadFile(String sourceFileUri, String judul) {


  countdown();
  // ip komputer server
  String upLoadServerUri = "http://tolong.hol.es/penilaian/upload.php";
  String fileName = sourceFileUri;

  HttpURLConnection conn = null;
  DataOutputStream dos = null;
  String lineEnd = "\r\n";
  String twoHyphens = "--";
  String boundary = "*****";
  int bytesRead, bytesAvailable, bufferSize;
  byte[] buffer;
  int maxBufferSize = 1 * 4096 * 4096;
  File sourceFile = new File(sourceFileUri);
  if (!sourceFile.isFile()) {
   Log.e("uploadFile", "Source File Does not exist");
   return 0;
  }
  try {
   FileInputStream fileInputStream = new FileInputStream(sourceFile);
   URL url = new URL(upLoadServerUri);
   conn = (HttpURLConnection) url.openConnection();
   conn.setDoInput(true); // Allow Inputs
   conn.setDoOutput(true); // Allow Outputs
   conn.setUseCaches(false); // Don't use a Cached Copy
   conn.setRequestMethod("POST");
   conn.setRequestProperty("Connection", "Keep-Alive");
   conn.setRequestProperty("Content-Type",
     "multipart/form-data;boundary=" + boundary);
   conn.setRequestProperty("uploaded_file", fileName);
   dos = new DataOutputStream(conn.getOutputStream());
   dos.writeBytes(twoHyphens + boundary + lineEnd);

   // untuk parameter keterangan
   dos.writeBytes("Content-Disposition: form-data; name=\"judul\""
     + lineEnd);
   dos.writeBytes(lineEnd);
   dos.writeBytes(judul);
   dos.writeBytes(lineEnd);
   dos.writeBytes(twoHyphens + boundary + lineEnd);

   // jika ingin menambahkan parameter baru, silahkan buat baris baru
   // lagi seperti berikut
   // dos.writeBytes("Content-Disposition: form-data; name=\"keterangan\""+
   // lineEnd);
   // dos.writeBytes(lineEnd);
   // dos.writeBytes(keterangan);
   // dos.writeBytes(lineEnd);
   // dos.writeBytes(twoHyphens + boundary + lineEnd);
   
   dos.writeBytes("Content-Disposition: form-data; name=\"id_guru\""+
		   lineEnd);
		   dos.writeBytes(lineEnd);
		   dos.writeBytes(id_guru);
		   dos.writeBytes(lineEnd);
		   dos.writeBytes(twoHyphens + boundary + lineEnd);

   dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
     + fileName + "\"" + lineEnd);
   dos.writeBytes(lineEnd);
   // create a buffer of maximum size
   bytesAvailable = fileInputStream.available();
   bufferSize = Math.min(bytesAvailable, maxBufferSize);
   buffer = new byte[bufferSize];
   // read file and write it into form...
   bytesRead = fileInputStream.read(buffer, 0, bufferSize);

   while (bytesRead > 0) {
    dos.write(buffer, 0, bufferSize);
    bytesAvailable = fileInputStream.available();
    bufferSize = Math.min(bytesAvailable, maxBufferSize);
    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
   }

   dos.writeBytes(lineEnd);
   dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

   serverResponseCode = conn.getResponseCode();
   String serverResponseMessage = conn.getResponseMessage();

   if (serverResponseCode == 200) {
    runOnUiThread(new Runnable() {
     public void run() {
      textViewKeterangan.setText("Keterangan : Upload Berhasil");
      Toast.makeText(RPP2.this, "Upload Berhasil.",
        Toast.LENGTH_SHORT).show();
      
      Intent intent = new Intent(RPP2.this, Daftar_RPP.class);
      startActivity(intent);
     }
    });
   }

   // close the streams //
   fileInputStream.close();
   dos.flush();
   dos.close();

  } catch (MalformedURLException ex) {
   hidepDialog();
   ex.printStackTrace();
   Toast.makeText(RPP2.this, "MalformedURLException",
     Toast.LENGTH_SHORT).show();
   Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
  } catch (Exception e) {
   hidepDialog();
   e.printStackTrace();
   Toast.makeText(RPP2.this, "Exception : " + e.getMessage(),
     Toast.LENGTH_SHORT).show();
   Log.e("Upload file to server Exception",
     "Exception : " + e.getMessage(), e);
  }
  hidepDialog();
  return serverResponseCode;

 }
 
 
 public void countdown()
 {
	 new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), "Batas waktu upload berakhir",Toast.LENGTH_SHORT).show();
				hidepDialog();
				onBackPressed();
			}
		},7000);
 }
 
 private void showpDialog() {
     if (!pDialog.isShowing())
         pDialog.show();
 }

 private void hidepDialog() {
     if (pDialog.isShowing())
         pDialog.dismiss();
 }

}