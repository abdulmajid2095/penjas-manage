package com.example.presensi;


import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;

public class Menuku extends Activity implements OnClickListener {

	Button m1,m2,m3,m4,m5,m6;
	Dataku data;
	private ProgressDialog pDialog;
	String lokasi_file="";
	String path;
	ProgressBar xku;
	private static final int REQUEST_CHOOSER = 1234;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_menuku);
		
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Logout ...");
        pDialog.setCancelable(false);
		
		data = Dataku.findById(Dataku.class, 1L);
		m1 = (Button) findViewById(R.id.menu_absensi);
		m1.setOnClickListener(this);
		m2 = (Button) findViewById(R.id.menu_penilaian);
		m2.setOnClickListener(this);
		m3 = (Button) findViewById(R.id.menu_data);
		m3.setOnClickListener(this);
		m4 = (Button) findViewById(R.id.menu_profil);
		m4.setOnClickListener(this);
		m5 = (Button) findViewById(R.id.menu_logout);
		m5.setOnClickListener(this);
		m6 = (Button) findViewById(R.id.menu_rpp);
		m6.setOnClickListener(this);
		
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.menu_absensi:
			Intent intent3 = new Intent(Menuku.this,Daftar_Kelas.class);
	    	intent3.putExtra("pilihan", "1");
	    	startActivity(intent3);
			break;
			
		case R.id.menu_penilaian:
			Intent intent2 = new Intent(Menuku.this,Daftar_Kelas.class);
	    	intent2.putExtra("pilihan", "2");
	    	startActivity(intent2);
			break;
			
		case R.id.menu_data:
			kelas();
			break;
	
		case R.id.menu_profil:
			Intent intent = new Intent(Menuku.this,Profil.class);
			startActivity(intent);
			
			break;
			
		case R.id.menu_logout:
			data.login=0;
			data.save();
			showpDialog();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					hidepDialog();
					Intent intent = new Intent(Menuku.this,Login.class);
					startActivity(intent);
				}
			},1200);
			break;
		
		case R.id.menu_rpp:
			//pilih_file();
			Intent intent5 = new Intent(Menuku.this,Daftar_RPP.class);
			startActivity(intent5);
			break;


		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}

	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    public void kelas()
    {
    	Intent intent = new Intent(Menuku.this,Daftar_Kelas.class);
    	intent.putExtra("pilihan", "3");
    	startActivity(intent);
    }
    
    
  
    
}


