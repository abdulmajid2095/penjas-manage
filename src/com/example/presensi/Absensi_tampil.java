package com.example.presensi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.presensi.R.id;

import android.R.layout;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.renderscript.Type;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Absensi_tampil extends Activity implements OnRefreshListener, OnClickListener{

	private SwipeRefreshLayout swipeRefreshLayout;
	ListView hasil_siswa;
	Dataku data;
	String[] id_siswa;
	String[] nama_siswa;
	String[] nis;
	int[] p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,p24,pbaru;
	EditText[] edit_nilai;
	String id_kelas,nama_kelas,tahun;
	TextView namakelas,namatahun;
	Button tambah;
	private ProgressDialog pDialog;
	int pertemuanke, jumlah_data;
	int pilihan;
	TextView kosong;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_absensi_tampil);
		
		Intent kelas = getIntent();
		nama_kelas = kelas.getStringExtra("nama_kelas");
		tahun = kelas.getStringExtra("tahun");
		id_kelas = kelas.getStringExtra("id_kelas");
		namakelas = (TextView)findViewById(R.id.siswa_nama_kelas);
		namakelas.setText("Nama Kelas : "+nama_kelas);
		namatahun = (TextView)findViewById(R.id.tahun);
		namatahun.setText(""+tahun);
		
		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.siswa_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		hasil_siswa = (ListView)findViewById(R.id.hasil_siswa);
		
		tambah = (Button) findViewById(R.id.tbl_tambah);
        tambah.setOnClickListener(this);
        
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mengganti Absensi ...");
        pDialog.setCancelable(true);
        
        kosong = (TextView) findViewById(R.id.teks_kosong);
        ambil_data();
	}

	
	
	
private void ambil_data() {
    	
    	String json_alamat = "http://tolong.hol.es/penilaian/daftar_siswa.php?id_kelas="+id_kelas;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        try {
                        	
                        	jumlah_data = response.length();
                        	if(jumlah_data==0)
                        	{
                        		kosong.setVisibility(View.VISIBLE);
                        	}
                        	else
                        	{
                        		kosong.setVisibility(View.GONE);
                        	}
                        	
                            
                        	id_siswa = new String[jumlah_data];
                        	nama_siswa = new String[jumlah_data];
                        	nis = new String[jumlah_data];
                        	p1 = new int[jumlah_data];
                        	p2 = new int[jumlah_data];
                        	p3 = new int[jumlah_data];
                        	p4 = new int[jumlah_data];
                        	p5 = new int[jumlah_data];
                        	p6= new int[jumlah_data];
                        	p7 = new int[jumlah_data];
                        	p8 = new int[jumlah_data];
                        	p9 = new int[jumlah_data];
                        	p10 = new int[jumlah_data];
                        	p11 = new int[jumlah_data];
                        	p12 = new int[jumlah_data];
                        	p13 = new int[jumlah_data];
                        	p14 = new int[jumlah_data];
                        	p15 = new int[jumlah_data];
                        	p16 = new int[jumlah_data];
                        	p17 = new int[jumlah_data];
                        	p18 = new int[jumlah_data];
                        	p19 = new int[jumlah_data];
                        	p20 = new int[jumlah_data];
                        	p21 = new int[jumlah_data];
                        	p22 = new int[jumlah_data];
                        	p23 = new int[jumlah_data];
                        	p24 = new int[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	id_siswa[i] = hasil.getString("id_siswa");
                                    nama_siswa[i] = hasil.getString("nama");
                                    nis[i] = hasil.getString("nis");
                                    p1[i] = Integer.parseInt(hasil.getString("p1"));
                                    p2[i] = Integer.parseInt(hasil.getString("p2"));
                                    p3[i] = Integer.parseInt(hasil.getString("p3"));
                                    p4[i] = Integer.parseInt(hasil.getString("p4"));
                                    p5[i] = Integer.parseInt(hasil.getString("p5"));
                                    p6[i] = Integer.parseInt(hasil.getString("p6"));
                                    p7[i] = Integer.parseInt(hasil.getString("p7"));
                                    p8[i] = Integer.parseInt(hasil.getString("p8"));
                                    p9[i] = Integer.parseInt(hasil.getString("p9"));
                                    p10[i] = Integer.parseInt(hasil.getString("p10"));
                                    p11[i] = Integer.parseInt(hasil.getString("p11"));
                                    p12[i] = Integer.parseInt(hasil.getString("p12"));
                                    p13[i] = Integer.parseInt(hasil.getString("p13"));
                                    p14[i] = Integer.parseInt(hasil.getString("p14"));
                                    p15[i] = Integer.parseInt(hasil.getString("p15"));
                                    p16[i] = Integer.parseInt(hasil.getString("p16"));
                                    p17[i] = Integer.parseInt(hasil.getString("p17"));
                                    p18[i] = Integer.parseInt(hasil.getString("p18"));
                                    p19[i] = Integer.parseInt(hasil.getString("p19"));
                                    p20[i] = Integer.parseInt(hasil.getString("p20"));
                                    p21[i] = Integer.parseInt(hasil.getString("p21"));
                                    p22[i] = Integer.parseInt(hasil.getString("p22"));
                                    p23[i] = Integer.parseInt(hasil.getString("p23"));
                                    p24[i] = Integer.parseInt(hasil.getString("p24"));
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Hasil tidak ditemukan",
                                    Toast.LENGTH_LONG).show();
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        //Toast.makeText(getApplicationContext(), ""+p4[0], Toast.LENGTH_LONG).show();
                        
                        Adapter_absensi adapter = new Adapter_absensi(getApplicationContext(), nama_siswa, nis, p1, p2, p3,p4, p5, p6, p7, p8, p9, p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,p24);
                		hasil_siswa.setAdapter(adapter);
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }




@Override
public void onRefresh() {
	// TODO Auto-generated method stub
	ambil_data();
}




@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.tbl_tambah:
		dialog_tambah();
		break;
}}




private void dialog_tambah() {

    // custom dialog
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.model_dialog_pilih_pertemuan);
    LinearLayout layout1 = (LinearLayout) dialog.findViewById(R.id.layout);
    for(int i=1;i<=24;i++)
    {
    	final int y = i;
    	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 110);
    	params2.topMargin=10;
    	params2.bottomMargin=10;
    	Button btn = new Button(this);
    	btn.setText("Pertemuan "+i);
    	btn.setTextColor(Color.WHITE);
    	btn.setBackgroundResource(R.drawable.tb);
    	btn.setLayoutParams(params2);
    	btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pertemuanke=y;
				pbaru = new int[jumlah_data];
				dialog.dismiss();
				cek();
				dialog_edit();
			}
		});
    	layout1.addView(btn);
    }
    dialog.show();
    
}

public void dialog_edit()
{
	// custom dialog
	final Dialog dialog = new Dialog(this);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.model_dialog_edit_pertemuan);
	TextView teks = (TextView) dialog.findViewById(R.id.nilaike);
	teks.setText("Pertemuan "+pertemuanke);
	
	for(int i=0;i<jumlah_data;i++)
	{
	
	
	LinearLayout layout1 = (LinearLayout) dialog.findViewById(R.id.layout);
	
	//Layout Nama dan Nilai
	LinearLayout layout2 = new LinearLayout(this);
	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	layout2.setLayoutParams(params2);
	layout2.setOrientation(LinearLayout.VERTICAL);
	
	
	//Layout Nama
	LinearLayout layout3 = new LinearLayout(this);
	LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	//params3.weight = 1.0f;
	//params3.gravity = Gravity.CENTER_VERTICAL;
	layout3.setLayoutParams(params3);
	
	
	TextView nama = new TextView(this);
	nama.setText(""+nama_siswa[i]);
	nama.setTextSize(16);
	LinearLayout.LayoutParams paramsx = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
	//paramsx.weight = 1.0f;
	nama.setLayoutParams(paramsx);
	
	//Layout Absen
	LinearLayout layout4 = new LinearLayout(this);
	LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	//params4.leftMargin=30;
	//params4.rightMargin=20;
	layout4.setLayoutParams(params4);
	layout4.setOrientation(LinearLayout.VERTICAL);
	
	final RadioGroup rg = new RadioGroup(getApplicationContext());
	rg.setId(i);
	RadioButton rb1=new RadioButton(getApplicationContext());
    rb1.setText("1. Hadir");
    rb1.setTextColor(Color.BLACK);
    RadioButton rb2=new RadioButton(getApplicationContext());
    rb2.setText("2. Sakit");
    rb2.setTextColor(Color.BLACK);
    RadioButton rb3=new RadioButton(getApplicationContext());
    rb3.setText("3. Ijin");
    rb3.setTextColor(Color.BLACK);
    RadioButton rb4=new RadioButton(getApplicationContext());
    rb4.setText("4. Tanpa Keterangan");
    rb4.setTextColor(Color.BLACK);
    
    rg.addView(rb1);
    rg.addView(rb2);
    rg.addView(rb3);
    rg.addView(rb4);
    
    if(pbaru[i]==1)
    {
    	rb1.setChecked(true);
    }
    else if(pbaru[i]==2)
    {
    	rb2.setChecked(true);
    }
    else if(pbaru[i]==3)
    {
    	rb3.setChecked(true);
    }
    else if(pbaru[i]==4)
    {
    	rb4.setChecked(true);
    }
    final int datake = i;
    rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			int childCount = group.getChildCount();
            for (int z = 0; z < childCount; z++) {
               RadioButton btn = (RadioButton) group.getChildAt(z);
               if (btn.getId() == checkedId) {
            	 
            	   String ans = btn.getText().toString();
            	   int ans2 = Integer.parseInt(ans.subSequence(0, 1).toString());
            	   //Toast.makeText(getApplicationContext(), "Data ke "+datake, Toast.LENGTH_LONG).show();
            	   //Toast.makeText(getApplicationContext(), ""+ans2, Toast.LENGTH_LONG).show();
            	   pbaru[datake] = ans2;

               }
            }
		}
	});
		

	LinearLayout layout5 = new LinearLayout(this);
	LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 2);
	params5.topMargin=10;
	params5.bottomMargin=10;
	layout5.setLayoutParams(params5);
	layout5.setBackgroundColor(Color.BLACK);
		
	
	layout1.addView(layout2);
	layout2.addView(layout3);
	layout2.addView(layout4);
	layout3.addView(nama);
	layout4.addView(rg);
	layout1.addView(layout5);

	
	if(i==jumlah_data-1)
	{
		Button btn = (Button) dialog.findViewById(R.id.dialog_pertemuan_simpan);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
							
				edit_pertemuan();
				dialog.dismiss();
				
			}
		});
	
	
	
	}}
	
	
	dialog.show(); 
}

public void edit_pertemuan()
{
	String url = "http://tolong.hol.es/penilaian/update_absen.php?jumlahdata="+jumlah_data+"&pertemuanke="+pertemuanke;
	
	for(int z=0;z<jumlah_data;z++)
	{
		url = url+"&id_siswa"+z+"="+id_siswa[z]+"&p"+z+"="+pbaru[z];
	}
	//Toast.makeText(getApplicationContext(), ""+url, Toast.LENGTH_LONG).show();
	JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    
                    try {
                            JSONObject login = (JSONObject) response
                                    .get(0);
                            
                            String peringatan = login.getString("validasi");
                            String cek1="1";
                            String cek2="2";
                            
                            if(peringatan.equals(cek1))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penggantian Absensi Gagal", Toast.LENGTH_SHORT).show(); 
                            }
                            if(peringatan.equals(cek2))
                            {
                            	hidepDialog();
                            	Toast.makeText(getApplicationContext(), "Penggantian Absensi Berhasil", Toast.LENGTH_LONG).show(); 
                            	onRefresh();
                            		
                            }
                            
                    
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                        		"Pendaftaran gagal, cek koneksi internet anda",
                                Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }

                    
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                    		"Pendaftaran gagal, cek koneksi internet anda", Toast.LENGTH_SHORT).show();
                    hidepDialog();
                }
            });
    // Adding request to request queue
	RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
    requestQueue.add(req);
    showpDialog();
    
    
}

private void showpDialog() {
    if (!pDialog.isShowing())
        pDialog.show();
}

private void hidepDialog() {
    if (pDialog.isShowing())
        pDialog.dismiss();
}


public void cek()
{
	if(pertemuanke==1)
	{
		pbaru=p1;
	}
	else if(pertemuanke==2)
	{
		pbaru=p2;
	}
	else if(pertemuanke==3)
	{
		pbaru=p3;
	}
	else if(pertemuanke==4)
	{
		pbaru=p4;
	}
	else if(pertemuanke==5)
	{
		pbaru=p5;
	}
	else if(pertemuanke==6)
	{
		pbaru=p6;
	}
	else if(pertemuanke==7)
	{
		pbaru=p7;
	}
	else if(pertemuanke==8)
	{
		pbaru=p8;
	}
	else if(pertemuanke==9)
	{
		pbaru=p9;
	}
	else if(pertemuanke==10)
	{
		pbaru=p10;
	}
	else if(pertemuanke==11)
	{
		pbaru=p11;
	}
	else if(pertemuanke==12)
	{
		pbaru=p12;
	}
	else if(pertemuanke==13)
	{
		pbaru=p13;
	}
	else if(pertemuanke==14)
	{
		pbaru=p14;
	}
	else if(pertemuanke==15)
	{
		pbaru=p15;
	}
	else if(pertemuanke==16)
	{
		pbaru=p16;
	}
	else if(pertemuanke==17)
	{
		pbaru=p17;
	}
	else if(pertemuanke==18)
	{
		pbaru=p18;
	}
	else if(pertemuanke==19)
	{
		pbaru=p19;
	}
	else if(pertemuanke==20)
	{
		pbaru=p20;
	}
	else if(pertemuanke==21)
	{
		pbaru=p21;
	}
	else if(pertemuanke==22)
	{
		pbaru=p22;
	}
	else if(pertemuanke==23)
	{
		pbaru=p23;
	}
	else
	{
		pbaru=p24;
	}
}

	
}
