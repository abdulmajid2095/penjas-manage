package com.example.presensi;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Profil extends Activity implements OnClickListener{

	private static final int SELECTED_PICTURE=1;
	Button simpan,ganti_foto;
	EditText nama,nip,pass1,pass2;
	Dataku data;
	Boolean valid1,valid2,valid3;
	CheckBox chek1;
	String k_nama,k_nip,k_pass1,k_pass2,tambahan="",lokasi_gambar="",nama_foto;
	private ProgressDialog pDialog;
	Bitmap gambar_untuk_upload;
	boolean foto_diganti=false;
	ImageView foto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_profil);
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Menyimpan");
        pDialog.setCancelable(false);
		chek1 = (CheckBox) findViewById(R.id.checkBox1);
		
		simpan = (Button) findViewById(R.id.profil_tbl_simpan);
		simpan.setOnClickListener(this);
		
		nama = (EditText) findViewById(R.id.profil_nama);
		nip = (EditText) findViewById(R.id.profil_nip);
		pass1 = (EditText) findViewById(R.id.profil_pass);
		pass2 = (EditText) findViewById(R.id.profil_pass2);
		
		data = Dataku.findById(Dataku.class, 1L);
		nama.setText(""+data.nama);
		nip.setText(""+data.nip);
		
		ganti_foto = (Button) findViewById(R.id.atur_ganti_foto);
		ganti_foto.setOnClickListener(this);
		
		foto = (ImageView)findViewById(R.id.atur_profil_gambar);
		
		Picasso.with(getApplicationContext())
		.load("http://tolong.hol.es/penilaian/foto/"+data.idku+".jpg")
		.memoryPolicy(MemoryPolicy.NO_CACHE)
		.networkPolicy(NetworkPolicy.NO_CACHE)
		.transform(new CircleTransform())
		.resize(300, 300)
        .centerCrop()
        .error(R.drawable.dp)
        .placeholder(R.drawable.dp)
        .into(foto);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.profil_tbl_simpan:
			pass1.setError(null);
			pass2.setError(null);
			nama.setError(null);
			nip.setError(null);
			k_nama=nama.getText().toString();
			k_nip=nip.getText().toString();
			k_pass1=pass1.getText().toString();
			k_pass2=pass2.getText().toString();
			cek();
			if(valid1==true&&valid2==true&&valid3==true)
			{
				k_nama = k_nama.replace(' ','+');
				k_nip = k_nip.replace(' ','+');
				k_pass1 = k_pass1.replace(' ','+');
				k_pass2 = k_pass2.replace(' ','+');
				
				if(chek1.isChecked())
				{
					if(k_pass2.equals(""))
					{
						pass2.setError("Password Baru tidak boleh kosong");
					}
					else
					{
						kirim();
						if(foto_diganti==true)
						{
							
							nama_foto=data.idku;
							new UploadGambar( gambar_untuk_upload, nama_foto).execute();
						}
					}
				}
				else
				{
					if(foto_diganti==true)
					{
						nama_foto=data.idku;
						new UploadGambar( gambar_untuk_upload, nama_foto).execute();
					}
					kirim();
				}
				
			}
			break;
			
		case R.id.atur_ganti_foto:
			pilih_galeri();
		break;

		}
	}
	
	public void cek()
	{
		String x = "";
		
		if(k_nama.equals(x))
		{
			nama.setError("Nama tidak boleh kosong");
			valid1=false;
		}
		else
		{
			valid1=true;
		}
		
		if(k_nip.equals(x))
		{
			nip.setError("NIP tidak boleh kosong");
			valid2=false;
		}
		else
		{
			valid2=true;
		}
		
		if(k_pass1.equals(x))
		{
			pass1.setError("Masukkan Password Anda");
			valid3=false;
		}
		else
		{
			if(pass1.getText().toString().equals(data.pass))
			{
				valid3=true;
			}
			else
			{
				valid3=false;
				pass1.setError("Password Salah");
			}
			
		}
	}
	
	
	public void kirim()
	{
		String url = "http://tolong.hol.es/penilaian/update_profil.php?id="+data.idku+"&nama="+k_nama+"&nip="+k_nip+"&pass="+k_pass2;
		
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                                JSONObject login = (JSONObject) response
                                        .get(0);
                                
                                String peringatan = login.getString("validasi");
                                String cek1="0";
                                String cek2="1";
                                
                                if(peringatan.equals(cek1))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Gagal Menyimpan", Toast.LENGTH_SHORT).show();
                                }
                                if(peringatan.equals(cek2))
                                {
                                	hidepDialog();
                                	Toast.makeText(getApplicationContext(), "Data Berhasil Diubah", Toast.LENGTH_SHORT).show();
                                	k_nama = k_nama.replace('+',' ');
                    				k_nip = k_nip.replace('+',' ');
                    				k_pass2 = k_pass2.replace('+',' ');
                                	data.nama=k_nama;
                                	data.nip=k_nip;
                                	if(chek1.isChecked())
                                	{
                                		data.pass=k_pass2;
                                	}
                                	data.save();
                                	
                                	onBackPressed();
                                }
                                
                        
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                            		"Periksa sambungan anda",
                                    Toast.LENGTH_LONG).show();
                            hidepDialog();
                        }
 
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        
                        Toast.makeText(getApplicationContext(),
                                "Periksa sambungan anda", Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
        showpDialog();
        
        
    }

	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    public void pilih_galeri()
	{
		/*startActivityForResult(galeri.openGalleryIntent(),GALERI_REQUEST);*/
		Intent galeri = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galeri, SELECTED_PICTURE);
	}
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	if (resultCode == RESULT_OK)
		{

			if(requestCode == SELECTED_PICTURE)
			{
				
				
				Uri uri = data.getData();
				String[]projection={MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
				cursor.moveToFirst();
				
				int columnIndex = cursor.getColumnIndex(projection[0]);
				lokasi_gambar = cursor.getString(columnIndex);
				cursor.close();
				
				
				
				Picasso.with(this)
				.load("file://" + lokasi_gambar)
				.noPlaceholder()
				.transform(new CircleTransform())
				.resize(300, 300)
		        .centerCrop()
		        .into(foto);
				
				foto_diganti=true;
				gambar_untuk_upload = BitmapFactory.decodeFile(lokasi_gambar);
				

			}
			
    }
}
    
    
    
    private class UploadGambar extends AsyncTask<Void, Void, Void>
	{
		Bitmap gambar;
		String nama;
		
		public UploadGambar(Bitmap gambar, String nama)
		{
			
			showpDialog();
			this.gambar = gambar;
			this.nama = nama;
		}
		
	

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			gambar.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);
			String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
			
			ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
			dataToSend.add(new BasicNameValuePair("gambar", encodedImage));
			dataToSend.add(new BasicNameValuePair("nama", nama));

			HttpParams httpRequestParams = getHttpRequestParams();
			HttpClient client = new DefaultHttpClient(httpRequestParams);
			HttpPost post = new HttpPost("http://tolong.hol.es/penilaian/upload_foto.php");
			
			
			try {
				post.setEntity(new UrlEncodedFormEntity(dataToSend));
				client.execute(post);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			hidepDialog();
		}
		
		
	}
	
	private HttpParams getHttpRequestParams()
	{
		HttpParams httpRequestParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpRequestParams, 1000 * 30);
		HttpConnectionParams.setSoTimeout(httpRequestParams, 1000 * 30);
		return httpRequestParams;
	}
    
}

