/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.presensi;

public final class R {
    public static final class attr {
    }
    public static final class bool {
        public static final int use_activity=0x7f050000;
        public static final int use_provider=0x7f050001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060002;
        public static final int activity_vertical_margin=0x7f060003;
        public static final int list_item_padding=0x7f060001;
        public static final int list_padding=0x7f060000;
    }
    public static final class drawable {
        public static final int bg1=0x7f020000;
        public static final int bg2=0x7f020001;
        public static final int bg3=0x7f020002;
        public static final int bg_login=0x7f020003;
        public static final int contoh=0x7f020004;
        public static final int dp=0x7f020005;
        public static final int edit=0x7f020006;
        public static final int edit1=0x7f020007;
        public static final int edit2=0x7f020008;
        public static final int hapus=0x7f020009;
        public static final int hapus1=0x7f02000a;
        public static final int hapus2=0x7f02000b;
        public static final int ic_chooser=0x7f02000c;
        public static final int ic_file=0x7f02000d;
        public static final int ic_folder=0x7f02000e;
        public static final int ic_launcher=0x7f02000f;
        public static final int ic_provider=0x7f020010;
        public static final int icedit=0x7f020011;
        public static final int icedit1=0x7f020012;
        public static final int icedit2=0x7f020013;
        public static final int ichapus=0x7f020014;
        public static final int ichapus1=0x7f020015;
        public static final int ichapus2=0x7f020016;
        public static final int kelas=0x7f020017;
        public static final int lg1=0x7f020018;
        public static final int lg2=0x7f020019;
        public static final int list=0x7f02001a;
        public static final int list1=0x7f02001b;
        public static final int list2=0x7f02001c;
        public static final int logo=0x7f02001d;
        public static final int m1=0x7f02001e;
        public static final int m7=0x7f02001f;
        public static final int n6=0x7f020020;
        public static final int n7=0x7f020021;
        public static final int pass=0x7f020022;
        public static final int pengaturan1=0x7f020023;
        public static final int profil1=0x7f020024;
        public static final int salah=0x7f020025;
        public static final int salah1=0x7f020026;
        public static final int salah2=0x7f020027;
        public static final int tambah=0x7f020028;
        public static final int tambah1=0x7f020029;
        public static final int tambah2=0x7f02002a;
        public static final int tb=0x7f02002b;
        public static final int tb1=0x7f02002c;
        public static final int tb2=0x7f02002d;
        public static final int tbl1=0x7f02002e;
        public static final int tbl2=0x7f02002f;
        public static final int tombol=0x7f020030;
        public static final int tulis=0x7f020031;
        public static final int tulis1=0x7f020032;
        public static final int tulis2=0x7f020033;
        public static final int unnes=0x7f020034;
        public static final int utama1=0x7f020035;
        public static final int utama2=0x7f020036;
        public static final int utama3=0x7f020037;
        public static final int warna1=0x7f020038;
        public static final int warna1a=0x7f020039;
        public static final int warna1b=0x7f02003a;
        public static final int warna2=0x7f02003b;
        public static final int warna2a=0x7f02003c;
        public static final int warna2b=0x7f02003d;
    }
    public static final class id {
        public static final int absen_alfa=0x7f0a004b;
        public static final int absen_hadir=0x7f0a004c;
        public static final int absen_ijin=0x7f0a004a;
        public static final int absen_persen=0x7f0a004d;
        public static final int absen_sakit=0x7f0a0049;
        public static final int action_settings=0x7f0a0075;
        public static final int atur_ganti_foto=0x7f0a001e;
        public static final int atur_profil_gambar=0x7f0a001d;
        public static final int buttonPilihFile=0x7f0a002c;
        public static final int buttonUpload=0x7f0a002d;
        public static final int checkBox1=0x7f0a0022;
        public static final int dialog_nilai_simpan=0x7f0a0055;
        public static final int dialog_penilaian1=0x7f0a005a;
        public static final int dialog_penilaian2=0x7f0a005b;
        public static final int dialog_penilaian3=0x7f0a005c;
        public static final int dialog_penilaian4=0x7f0a005d;
        public static final int dialog_penilaian5=0x7f0a005e;
        public static final int dialog_penilaian6=0x7f0a005f;
        public static final int dialog_pertemuan_simpan=0x7f0a0056;
        public static final int dialog_tambah_kelas_edittext=0x7f0a0062;
        public static final int dialog_tambah_kelas_edittext2=0x7f0a0063;
        public static final int dialog_tambah_kelas_edittext3=0x7f0a0064;
        public static final int dialog_tambah_kelas_edittext4=0x7f0a0065;
        public static final int dialog_tambah_kelas_simpan=0x7f0a0066;
        public static final int dialog_tambah_siswa_edittext1=0x7f0a0068;
        public static final int dialog_tambah_siswa_edittext2=0x7f0a0067;
        public static final int dialog_tambah_siswa_simpan=0x7f0a0069;
        public static final int edit=0x7f0a0060;
        public static final int editTextJudul=0x7f0a0029;
        public static final int edittext=0x7f0a004e;
        public static final int edittext2=0x7f0a004f;
        public static final int edittext3=0x7f0a0050;
        public static final int edittext4=0x7f0a0051;
        public static final int gambar_pencarian_user=0x7f0a006a;
        public static final int hapus=0x7f0a0061;
        public static final int hapus_teks=0x7f0a0057;
        public static final int hasil_kelas=0x7f0a000e;
        public static final int hasil_siswa=0x7f0a0003;
        public static final int kelas_swipe_refresh_layout=0x7f0a000d;
        public static final int layout=0x7f0a0054;
        public static final int login_nip=0x7f0a000f;
        public static final int login_pass=0x7f0a0010;
        public static final int login_tbl_keluar=0x7f0a0013;
        public static final int login_tbl_login=0x7f0a0011;
        public static final int login_tbl_signup=0x7f0a0012;
        public static final int logo=0x7f0a0015;
        public static final int menu_absensi=0x7f0a0017;
        public static final int menu_data=0x7f0a0016;
        public static final int menu_logout=0x7f0a001b;
        public static final int menu_penilaian=0x7f0a0018;
        public static final int menu_profil=0x7f0a001a;
        public static final int menu_rpp=0x7f0a0019;
        public static final int nama_kelas=0x7f0a006b;
        public static final int nilaike=0x7f0a0053;
        public static final int pertemuan1=0x7f0a0031;
        public static final int pertemuan10=0x7f0a003a;
        public static final int pertemuan11=0x7f0a003b;
        public static final int pertemuan12=0x7f0a003c;
        public static final int pertemuan13=0x7f0a003d;
        public static final int pertemuan14=0x7f0a003e;
        public static final int pertemuan15=0x7f0a003f;
        public static final int pertemuan16=0x7f0a0040;
        public static final int pertemuan17=0x7f0a0041;
        public static final int pertemuan18=0x7f0a0042;
        public static final int pertemuan19=0x7f0a0043;
        public static final int pertemuan2=0x7f0a0032;
        public static final int pertemuan20=0x7f0a0044;
        public static final int pertemuan21=0x7f0a0045;
        public static final int pertemuan22=0x7f0a0046;
        public static final int pertemuan23=0x7f0a0047;
        public static final int pertemuan24=0x7f0a0048;
        public static final int pertemuan3=0x7f0a0033;
        public static final int pertemuan4=0x7f0a0034;
        public static final int pertemuan5=0x7f0a0035;
        public static final int pertemuan6=0x7f0a0036;
        public static final int pertemuan7=0x7f0a0037;
        public static final int pertemuan8=0x7f0a0038;
        public static final int pertemuan9=0x7f0a0039;
        public static final int profil_nama=0x7f0a0020;
        public static final int profil_nip=0x7f0a001f;
        public static final int profil_pass=0x7f0a0021;
        public static final int profil_pass2=0x7f0a0023;
        public static final int profil_tbl_simpan=0x7f0a0024;
        public static final int progressBar1=0x7f0a0014;
        public static final int rpp_edittext=0x7f0a0025;
        public static final int rpp_pilih=0x7f0a0027;
        public static final int rpp_upload=0x7f0a0028;
        public static final int signup_nama=0x7f0a0006;
        public static final int signup_nip=0x7f0a0007;
        public static final int signup_pass=0x7f0a0008;
        public static final int signup_pass2=0x7f0a0009;
        public static final int signup_tbl_keluar=0x7f0a000c;
        public static final int signup_tbl_login=0x7f0a000b;
        public static final int signup_tbl_signup=0x7f0a000a;
        public static final int simpan=0x7f0a0052;
        public static final int siswa_keterangan=0x7f0a0074;
        public static final int siswa_kkm=0x7f0a0073;
        public static final int siswa_n1=0x7f0a006c;
        public static final int siswa_n2=0x7f0a006d;
        public static final int siswa_n3=0x7f0a006e;
        public static final int siswa_n4=0x7f0a006f;
        public static final int siswa_n5=0x7f0a0070;
        public static final int siswa_n6=0x7f0a0071;
        public static final int siswa_nama=0x7f0a0030;
        public static final int siswa_nama_kelas=0x7f0a0000;
        public static final int siswa_nis=0x7f0a002f;
        public static final int siswa_no=0x7f0a002e;
        public static final int siswa_rata=0x7f0a0072;
        public static final int siswa_swipe_refresh_layout=0x7f0a0001;
        public static final int siswa_tahun=0x7f0a001c;
        public static final int tahun=0x7f0a0002;
        public static final int tbl_tambah=0x7f0a0005;
        public static final int teks_kosong=0x7f0a0004;
        public static final int teks_path=0x7f0a0026;
        public static final int textViewKeterangan=0x7f0a002b;
        public static final int textViewPath=0x7f0a002a;
        public static final int tidak=0x7f0a0059;
        public static final int ya=0x7f0a0058;
    }
    public static final class layout {
        public static final int activity_absensi_tampil=0x7f030000;
        public static final int activity_daftar=0x7f030001;
        public static final int activity_daftar__kelas=0x7f030002;
        public static final int activity_daftar__rpp=0x7f030003;
        public static final int activity_daftar__siswa=0x7f030004;
        public static final int activity_login=0x7f030005;
        public static final int activity_main=0x7f030006;
        public static final int activity_menuku=0x7f030007;
        public static final int activity_penilaian_tampil=0x7f030008;
        public static final int activity_profil=0x7f030009;
        public static final int activity_rpp=0x7f03000a;
        public static final int activity_rpp2=0x7f03000b;
        public static final int file=0x7f03000c;
        public static final int model_absensi=0x7f03000d;
        public static final int model_dialog_edit_kelas=0x7f03000e;
        public static final int model_dialog_edit_nilai=0x7f03000f;
        public static final int model_dialog_edit_pertemuan=0x7f030010;
        public static final int model_dialog_edit_rpp=0x7f030011;
        public static final int model_dialog_edit_siswa=0x7f030012;
        public static final int model_dialog_hapus=0x7f030013;
        public static final int model_dialog_pilih_nilai=0x7f030014;
        public static final int model_dialog_pilih_pertemuan=0x7f030015;
        public static final int model_dialog_pilihan=0x7f030016;
        public static final int model_dialog_tambah_kelas=0x7f030017;
        public static final int model_dialog_tambah_siswa=0x7f030018;
        public static final int model_kelas=0x7f030019;
        public static final int model_penilaian=0x7f03001a;
        public static final int model_siswa=0x7f03001b;
    }
    public static final class menu {
        public static final int absensi_tampil=0x7f090000;
        public static final int daftar=0x7f090001;
        public static final int daftar__kelas=0x7f090002;
        public static final int daftar__rp=0x7f090003;
        public static final int daftar__siswa=0x7f090004;
        public static final int login=0x7f090005;
        public static final int main=0x7f090006;
        public static final int menuku=0x7f090007;
        public static final int penilaian_tampil=0x7f090008;
        public static final int profil=0x7f090009;
        public static final int rp=0x7f09000a;
        public static final int rpp2=0x7f09000b;
        public static final int splash=0x7f09000c;
    }
    public static final class string {
        public static final int action_settings=0x7f070006;
        public static final int app_name=0x7f070005;
        public static final int choose_file=0x7f070002;
        public static final int chooser_title=0x7f070013;
        public static final int empty_directory=0x7f070000;
        public static final int error_selecting_file=0x7f070003;
        public static final int hello_world=0x7f070007;
        public static final int internal_storage=0x7f070004;
        public static final int storage_removed=0x7f070001;
        public static final int title_activity_absensi_tampil=0x7f07000f;
        public static final int title_activity_daftar=0x7f07000b;
        public static final int title_activity_daftar__kelas=0x7f07000c;
        public static final int title_activity_daftar__rpp=0x7f070011;
        public static final int title_activity_daftar__siswa=0x7f07000e;
        public static final int title_activity_login=0x7f07000a;
        public static final int title_activity_menuku=0x7f070008;
        public static final int title_activity_penilaian_tampil=0x7f070010;
        public static final int title_activity_profil=0x7f07000d;
        public static final int title_activity_rpp=0x7f070012;
        public static final int title_activity_rpp2=0x7f070014;
        public static final int title_activity_splash=0x7f070009;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080001;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080002;
        public static final int fileChooserName=0x7f080000;
    }
    public static final class xml {
        public static final int mimetypes=0x7f040000;
    }
}
